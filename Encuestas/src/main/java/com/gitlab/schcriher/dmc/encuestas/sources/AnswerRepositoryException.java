package com.gitlab.schcriher.dmc.encuestas.sources;

import com.gitlab.schcriher.dmc.encuestas.shared.AbstractBaseException;

public class AnswerRepositoryException extends AbstractBaseException {

    private static final long serialVersionUID = 1L;

    public AnswerRepositoryException() {
        this(null, null);
    }

    public AnswerRepositoryException(String detail) {
        super(detail, null);
    }

    public AnswerRepositoryException(Throwable cause) {
        super(null, cause);
    }

    public AnswerRepositoryException(String detail, Throwable cause) {
        super(detail, cause);
    }
}
