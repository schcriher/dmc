package com.gitlab.schcriher.dmc.encuestas.sources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.domain.Choice;
import com.gitlab.schcriher.dmc.encuestas.domain.Selection;

public class AnswerJdbcRepository implements AnswerRepository {

    private static final String ERROR = "could not be saved in the database";

    private Connection connection;

    public AnswerJdbcRepository(Connection connection) {
        this.connection = connection;
    }

    private static String getSQL(String ini, String part, String sep, String end, int count) {
        final StringBuilder sql = new StringBuilder(ini);
        for (int i = 0; i < count; i++) {
            sql.append(part);
            if (i != count - 1) {
                sql.append(sep);
            }
        }
        sql.append(end);
        return sql.toString();
    }

    public static Object[] getSliceOfArray(Object[] arr, int start, int end) {
        Object[] slice = new Object[end - start];
        for (int i = 0; i < slice.length; i++) {
            slice[i] = arr[start + i];
        }
        return slice;
    }

    private static void setValues(PreparedStatement statement, Object[] args) throws SQLException {
        int index = 1;
        for (Object arg : args) {
            statement.setObject(index, arg);
            index++;
        }
    }

    private Integer getId(Object... args) throws AnswerRepositoryException {
        // args: table, names..., values...
        final int divider = 2;
        String sql = getSQL("SELECT t.id AS tid FROM %s AS t WHERE",
                            " t.%s = ?",
                            " AND",
                            ";",
                            (args.length - 1) / divider);
        final int cut = (args.length + 1) / divider;
        sql = String.format(sql, getSliceOfArray(args, 0, cut));

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            setValues(statement, getSliceOfArray(args, cut, args.length));
            ResultSet result = statement.executeQuery();
            result.next();
            int id = Integer.parseInt(result.getString("tid"));
            result.close();
            return id;
        } catch (Exception e) {
            throw new AnswerRepositoryException(ERROR, e);
        }
    }

    private void insertValues(String table, Object... args) throws AnswerRepositoryException {
        // args: table, values...
        String sql = getSQL("INSERT INTO %s VALUES (",
                            "?",
                            ", ",
                            ");",
                            args.length);
        sql = String.format(sql, table);
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            setValues(statement, args);
            statement.execute();
        } catch (Exception e) {
            throw new AnswerRepositoryException(ERROR, e);
        }
    }

    @Override
    public boolean existsByName(String name) {
        final String sql = "SELECT FROM answer AS a WHERE a.name = ?";
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                result.close();
                return true;
            }
            result.close();
        } catch (SQLException e) {
            // No logger system was implemented
        }
        return false;
    }

    @Override
    public Answer createByName(String name) throws AnswerRepositoryException {
        final String sql = "SELECT FROM answer AS a WHERE a.name = ?";

        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                result.close();
                throw new AnswerRepositoryException("answer already exists");
            }
            result.close();
        } catch (SQLException e) {
            throw new AnswerRepositoryException(e);
        }

        Answer answer = new Answer();
        answer.setName(name);
        return answer;
    }

    @Override
    public void save(Answer answer) throws AnswerRepositoryException {
        try {
            insertValues("answer(name, author, datetime, survey_id)",
                         answer.getName(),
                         answer.getAuthor(),
                         answer.getDateTime(),
                         answer.getSurvey().getId());
            int aid = getId("answer", "name", answer.getName());
            answer.setId(aid);

            for (Selection selection : answer.getSelections()) {

                int qid = selection.getQuestion().getId();
                insertValues("selection(answer_id, question_id)", aid, qid);
                int ssid = getId("selection", "answer_id", "question_id", aid, qid);

                for (Choice choice : selection.getChoices()) {
                    int cid = choice.getId();
                    insertValues("selection_choice(selection_id, choice_id)", ssid, cid);
                }
            }
            connection.commit();
        } catch (SQLException | AnswerRepositoryException e) {
            try {
                connection.rollback();
            } catch (SQLException e2) {
                throw new AnswerRepositoryException(e2);
            }
            throw new AnswerRepositoryException(e);
        }
    }

    @Override
    public List<Answer> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Answer findByName(String name) {
        throw new UnsupportedOperationException();
    }
}
