package com.gitlab.schcriher.dmc.encuestas.shared;

import java.io.InputStream;

public final class Resources {

    private Resources() {/* hidden */}

    public static InputStream getResourceAsStream(String path) {
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream;
        if (loader != null) {
            stream = loader.getResourceAsStream(path);
            if (stream != null) {
                return stream;
            }
        }
        loader = Resources.class.getClassLoader();
        if (loader != null) {
            stream = loader.getResourceAsStream(path);
            if (stream != null) {
                return stream;
            }
        }
        return ClassLoader.getSystemResourceAsStream(path);
    }
}
