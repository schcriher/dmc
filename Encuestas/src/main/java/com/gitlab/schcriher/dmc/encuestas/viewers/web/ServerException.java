package com.gitlab.schcriher.dmc.encuestas.viewers.web;

import com.gitlab.schcriher.dmc.encuestas.shared.AbstractBaseException;

public class ServerException extends AbstractBaseException {

    private static final long serialVersionUID = 1L;

    public static class MissingIdException extends ServerException {

        private static final long serialVersionUID = 1L;

        public MissingIdException() {
            this(null, null);
        }

        public MissingIdException(String detail) {
            super(detail, null);
        }

        public MissingIdException(Throwable cause) {
            super(null, cause);
        }

        public MissingIdException(String detail, Throwable cause) {
            super(detail, cause);
        }
    }

    public ServerException() {
        this(null, null);
    }

    public ServerException(String detail) {
        super(detail, null);
    }

    public ServerException(Throwable cause) {
        super(null, cause);
    }

    public ServerException(String detail, Throwable cause) {
        super(detail, cause);
    }
}
