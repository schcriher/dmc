package com.gitlab.schcriher.dmc.encuestas.shared;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;

public class SurveyJsonSerializer extends StdSerializer<Survey> {

    private static final long serialVersionUID = 1L;
    public static final Version version = new Version(1, 0, 0, null, null, null);

    public SurveyJsonSerializer() {
        this(null);
    }

    public SurveyJsonSerializer(Class<Survey> survey) {
        super(survey);
    }

    public static SimpleModule getModule() {
        SimpleModule module = new SimpleModule("SurveyJsonSerializer", version);
        module.addSerializer(Survey.class, new SurveyJsonSerializer());
        return module;
    }

    @Override
    public void serialize(Survey survey,
                          JsonGenerator jsonGenerator,
                          SerializerProvider serializer) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", survey.getName());
        jsonGenerator.writeStringField("type", survey.getType().name());
        jsonGenerator.writeEndObject();
    }
}
