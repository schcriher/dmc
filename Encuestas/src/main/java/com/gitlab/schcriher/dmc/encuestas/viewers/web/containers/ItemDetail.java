package com.gitlab.schcriher.dmc.encuestas.viewers.web.containers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ItemDetail {

    private String name;
    private String badge;
    private boolean single;
    private boolean correct;
    private boolean selected;
    private boolean evaluative;
    private boolean approved;
    private List<ItemDetail> subItems;

    public ItemDetail(String name) {
        this(name, "", false, false, false, false, false);
    }

    public ItemDetail(String name, String badge) {
        this(name, badge, false, false, false, false, false);
    }

    public ItemDetail(String name, String badge, boolean single) {
        this(name, badge, single, false, false, false, false);
    }

    public ItemDetail(String name,
                      boolean correct,
                      boolean selected,
                      boolean evaluative,
                      boolean approved) {
        this(name, "", false, correct, selected, evaluative, approved);
    }

    public ItemDetail(String name,
                      String badge,
                      boolean single,
                      boolean correct,
                      boolean selected,
                      boolean evaluative,
                      boolean approved) {
        this.name = name;
        this.badge = badge;
        this.single = single;
        this.correct = correct;
        this.selected = selected;
        this.evaluative = evaluative;
        this.approved = approved;
        subItems = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public String getBadge() {
        return badge;
    }

    public boolean isSingle() {
        return single;
    }

    public boolean isCorrect() {
        return correct;
    }

    public boolean isSelected() {
        return selected;
    }

    public boolean isEvaluative() {
        return evaluative;
    }

    public boolean isApproved() {
        return approved;
    }

    public List<ItemDetail> getSubItems() {
        return Collections.unmodifiableList(subItems);
    }

    public void addSubItem(ItemDetail item) {
        subItems.add(item);
    }
}
