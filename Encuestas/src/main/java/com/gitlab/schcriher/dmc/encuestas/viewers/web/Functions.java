package com.gitlab.schcriher.dmc.encuestas.viewers.web;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.http.MimeTypes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.domain.Choice;
import com.gitlab.schcriher.dmc.encuestas.domain.Question;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionException;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionType;
import com.gitlab.schcriher.dmc.encuestas.domain.Selection;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyException;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyType;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyRepositoryException;
import com.gitlab.schcriher.dmc.encuestas.viewers.web.ServerException.MissingIdException;
import com.gitlab.schcriher.dmc.encuestas.viewers.web.containers.ItemDetail;
import com.gitlab.schcriher.dmc.encuestas.viewers.web.containers.ItemList;

import spark.Response;
import spark.Route;
import spark.Spark;

public final class Functions {

    public static final String ERROR = "internal code error";
    public static final Locale LOCALE = Locale.forLanguageTag("es");
    public static final String QC_ID_REGEX = "^q(?<qid>\\d+)c(?<cid>\\d+)$";
    public static final Pattern QC_ID_PATTERN = Pattern.compile(QC_ID_REGEX);

    private Functions() {/* hidden */}

    public static String firstLetterCap(String text) {
        return text.substring(0, 1).toUpperCase(LOCALE) + text.substring(1);
    }

    public static void redirectTrailingSlash(String verb, String path) {
        try {
            Route route = (request, response) -> {
                response.redirect(path, HttpStatus.PERMANENT_REDIRECT_308);
                return null;
            };
            Method method = Spark.class.getMethod(verb, String.class, Route.class);
            method.invoke(null, path + "/", route);

        } catch (NoSuchMethodException |
                 SecurityException |
                 IllegalAccessException |
                 IllegalArgumentException |
                 InvocationTargetException e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public static List<ItemList> getSurveyItemList(SurveyRepository surveyRepo) {
        List<ItemList> list = new ArrayList<>();
        for (Survey survey : surveyRepo.getAll()) {
            String name = survey.getName();
            list.add(new ItemList(name,
                                  Path.Web.SURVEY_NAME.replace(Path.NAME_VAR, name),
                                  survey.getQuestions()
                                        .stream()
                                        .count()));
        }
        return list;
    }

    public static List<ItemDetail> getSurveyItemDetail(Survey survey) {
        List<ItemDetail> list = new ArrayList<>();
        for (Question question : survey.getQuestions()) {
            ItemDetail item = new ItemDetail(question.getText(),
                                             question.getType().getLocalizedType());
            list.add(item);
            for (Choice choice : question.getChoices()) {
                item.addSubItem(new ItemDetail(choice.getText()));
            }
        }
        return list;
    }

    public static List<ItemList> getAnswerItemList(AnswerRepository answerRepo) {
        List<ItemList> list = new ArrayList<>();
        for (Answer answer : answerRepo.getAll()) {
            String name = answer.getName();
            list.add(new ItemList(name,
                                  Path.Web.ANSWER_NAME.replace(Path.NAME_VAR, name),
                                  answer.getSelections()
                                        .stream()
                                        .map(Selection::getChoices)
                                        .mapToInt(List<Choice>::size)
                                        .sum()));
        }
        return list;
    }

    public static List<ItemDetail> getAnswerItemDetail(Answer answer) {
        List<ItemDetail> list = new ArrayList<>();
        Map<Question, List<Choice>> qsMap = answer.getSelections()
                                                  .stream()
                                                  .collect(Collectors.toMap(Selection::getQuestion,
                                                                            Selection::getChoices));

        boolean evaluative = answer.getSurvey().getType() == SurveyType.EVALUATION;

        for (Question question : answer.getSurvey().getQuestions()) {
            ItemDetail item = new ItemDetail(question.getText(),
                                             question.getType().getLocalizedType(),
                                             question.getType() == QuestionType.SINGLE);

            for (Choice choice : question.getChoices()) {
                boolean correct = question.isCorrectChoice(choice);
                boolean selected = isSelected(qsMap, question, choice);
                boolean approved = correct == selected;
                item.addSubItem(new ItemDetail(choice.getText(),
                                               correct, selected, evaluative, approved));
            }
            list.add(item);
        }
        return list;
    }

    public static Answer getAnswerFromBody(SurveyRepository surveyRepo,
                                           String body) throws ServerException {
        List<NameValuePair> params = URLEncodedUtils.parse(body, StandardCharsets.UTF_8);
        Map<Integer, List<Integer>> ids = new HashMap<>();
        Answer answer = new Answer();
        Survey survey = null;

        for (final NameValuePair param : params) {
            switch (param.getName()) {

                case "survey_name":
                    try {
                        survey = surveyRepo.findByName(param.getValue());
                    } catch (SurveyRepositoryException e) {
                        throw new ServerException("survey answered not found", e);
                    }
                    break;

                case "answer":
                    answer.setName(param.getValue().trim());
                    break;

                case "author":
                    answer.setAuthor(param.getValue().trim());
                    break;

                case "selection":
                    Matcher matcher = QC_ID_PATTERN.matcher(param.getValue());
                    if (matcher.matches()) {
                        Integer qid = Integer.parseInt(matcher.group("qid"));
                        Integer cid = Integer.parseInt(matcher.group("cid"));
                        if (ids.containsKey(qid)) {
                            ids.get(qid).add(cid);
                        } else {
                            List<Integer> array = new ArrayList<>();
                            array.add(cid);
                            ids.put(qid, array);
                        }
                    }
                    break;
            }
        }
        if (survey != null) {
            fillAnswerElections(answer, survey, ids);
            answer.setSurvey(survey);
            return answer;
        }
        throw new ServerException("survey answered not found");
    }

    public static void fillAnswerElections(Answer answer,
                                           Survey survey,
                                           Map<Integer, List<Integer>> ids) throws MissingIdException {

        for (Map.Entry<Integer, List<Integer>> entry : ids.entrySet()) {
            Question question;
            try {
                question = survey.getQuestionById(entry.getKey());
            } catch (SurveyException e) {
                throw new MissingIdException("missing question identifier", e);
            }
            Selection selection = new Selection();
            selection.setAnswer(answer);
            selection.setQuestion(question);
            for (Integer cid : entry.getValue()) {
                Choice choice;
                try {
                    choice = question.getChoiceById(cid);
                } catch (QuestionException e) {
                    throw new MissingIdException("missing choice identifier", e);
                }
                selection.addChoice(choice);
            }
            answer.addSelection(selection);
        }
    }

    public static boolean isSelected(Map<Question, List<Choice>> qc,
                                     Question question,
                                     Choice choice) {
        try {
            return qc.get(question).contains(choice);
        } catch (NullPointerException e) {
            // Is not strictly necessary to logger this exception
            return false;
        }
    }

    public static String processReservation(Response response,
                                            boolean reserved) throws ServerException {
        return processReservation(response, reserved, null);
    }

    public static String processReservation(Response response,
                                            boolean reserved,
                                            String encodeName) throws ServerException {
        try {
            ObjectMapper mapper = new ObjectMapper();
            ObjectNode data = mapper.createObjectNode();
            int status;
            if (reserved) {
                if (encodeName != null) {
                    String url = Path.Web.ANSWER_NAME_RESERVE.replace(Path.NAME_VAR,
                                                                      encodeName);
                    ObjectNode reserve = mapper.createObjectNode();
                    reserve.put("refresh_url", url);
                    reserve.put("refresh_method", "POST");
                    reserve.put("refresh_millisecond", NameCache.MILLISECOND_RESERVE_REFRESH);
                    data.set("reserve", reserve);
                }
                status = HttpStatus.ACCEPTED_202;
            } else {
                status = HttpStatus.NOT_ACCEPTABLE_406;
            }
            data.put("reserve_name", reserved);
            response.status(status);
            response.type(MimeTypes.Type.APPLICATION_JSON.asString());
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(data);
        } catch (JsonProcessingException e) {
            throw new ServerException(e);
        }
    }
}
