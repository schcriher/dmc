package com.gitlab.schcriher.dmc.encuestas.sources;

import java.util.List;

import com.gitlab.schcriher.dmc.encuestas.domain.Survey;

public interface SurveyRepository {

    Survey findByName(String name) throws SurveyRepositoryException;

    List<Survey> getAll();

}
