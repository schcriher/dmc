package com.gitlab.schcriher.dmc.encuestas.sources;

import com.gitlab.schcriher.dmc.encuestas.shared.AbstractBaseException;

public class SurveyRepositoryException extends AbstractBaseException {

    private static final long serialVersionUID = 1L;

    public SurveyRepositoryException() {
        this(null, null);
    }

    public SurveyRepositoryException(String detail) {
        super(detail, null);
    }

    public SurveyRepositoryException(Throwable cause) {
        super(null, cause);
    }

    public SurveyRepositoryException(String detail, Throwable cause) {
        super(detail, cause);
    }
}
