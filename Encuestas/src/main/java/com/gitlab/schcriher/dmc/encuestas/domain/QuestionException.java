package com.gitlab.schcriher.dmc.encuestas.domain;

import com.gitlab.schcriher.dmc.encuestas.shared.AbstractBaseException;

public class QuestionException extends AbstractBaseException {

    private static final long serialVersionUID = 1L;

    public QuestionException() {
        this(null, null);
    }

    public QuestionException(String detail) {
        super(detail, null);
    }

    public QuestionException(Throwable cause) {
        super(null, cause);
    }

    public QuestionException(String detail, Throwable cause) {
        super(detail, cause);
    }
}
