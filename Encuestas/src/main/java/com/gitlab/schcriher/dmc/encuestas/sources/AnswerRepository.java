package com.gitlab.schcriher.dmc.encuestas.sources;

import java.util.List;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;

public interface AnswerRepository {

    boolean existsByName(String name);

    Answer createByName(String name) throws AnswerRepositoryException;

    void save(Answer answer) throws AnswerRepositoryException;

    Answer findByName(String name) throws AnswerRepositoryException;

    List<Answer> getAll();

}
