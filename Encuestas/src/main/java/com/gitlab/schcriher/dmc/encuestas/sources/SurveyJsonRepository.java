package com.gitlab.schcriher.dmc.encuestas.sources;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;

public class SurveyJsonRepository implements SurveyRepository {

    private final File directory;

    public SurveyJsonRepository(File directory) {
        this.directory = directory;
    }

    @Override
    public Survey findByName(String name) throws SurveyRepositoryException {
        File file = new File(directory.getAbsoluteFile(), name + ".json");
        if (!file.exists()) {
            throw new SurveyRepositoryException(String.format("Survey '%s' does not exist", name));
        }
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(file, Survey.class);
        } catch (JsonParseException e) {
            throw new SurveyRepositoryException("json badly formed", e);
        } catch (JsonMappingException e) {
            throw new SurveyRepositoryException("json does not respect the structure", e);
        } catch (IOException e) {
            throw new SurveyRepositoryException(e);
        }
    }

    @Override
    public List<Survey> getAll() {
        throw new UnsupportedOperationException();
    }
}
