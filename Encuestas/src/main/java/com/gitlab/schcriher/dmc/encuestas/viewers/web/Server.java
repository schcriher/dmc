package com.gitlab.schcriher.dmc.encuestas.viewers.web;

import static spark.Spark.get;
import static spark.Spark.internalServerError;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFiles;
import static spark.debug.DebugScreen.enableDebugScreen;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.eclipse.jetty.http.HttpStatus;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyType;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerJpaRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerRepositoryException;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyJpaRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyRepositoryException;
import com.gitlab.schcriher.dmc.encuestas.viewers.web.ServerException.MissingIdException;
import com.gitlab.schcriher.dmc.encuestas.viewers.web.containers.ItemAction;

import spark.ModelAndView;
import spark.Response;
import spark.template.thymeleaf.ThymeleafTemplateEngine;

public class Server {

    private static final long STATIC_EXPIRE_SECONDS = 300;
    private static final String UTF8 = StandardCharsets.UTF_8.toString();

    private final EntityManagerFactory factory;
    private final EntityManager manager;
    private final SurveyRepository surveyRepo;
    private final AnswerRepository answerRepo;

    private final ThymeleafTemplateEngine template;
    private final NameCache nameCache;

    public Server() {
        Map<String, String> env = System.getenv();

        int port = Integer.parseInt(env.getOrDefault("PORT", "4567"));
        boolean dev = Boolean.parseBoolean(env.getOrDefault("DEVELOPMENT", "false"));

        Map<String, String> cfg = new HashMap<>();
        cfg.put("hibernate.connection.url", env.get("JDBC_DATABASE_URL"));
        cfg.put("hibernate.connection.username", env.get("JDBC_DATABASE_USERNAME"));
        cfg.put("hibernate.connection.password", env.get("JDBC_DATABASE_PASSWORD"));
        factory = Persistence.createEntityManagerFactory("db", cfg);
        manager = factory.createEntityManager();
        surveyRepo = new SurveyJpaRepository(manager);
        answerRepo = new AnswerJpaRepository(manager);

        template = new ThymeleafTemplateEngine();
        nameCache = new NameCache();

        if (dev) {
            enableDebugScreen();
            String projectDir = System.getProperty("user.dir");
            String staticDir = "/src/main/resources/static/";
            staticFiles.externalLocation(projectDir + staticDir);
            staticFiles.expireTime(1);
        } else {
            staticFiles.location("/static");
            staticFiles.expireTime(STATIC_EXPIRE_SECONDS);
        }
        port(port);
    }

    public void setRoutes() {
        final String root = "/";

        if (!Path.Web.HOME.equals(root)) {
            get(root, (request, response) -> {
                response.redirect(Path.Web.HOME, HttpStatus.PERMANENT_REDIRECT_308);
                return null;
            });
            Functions.redirectTrailingSlash("get", Path.Web.HOME);
        }

        get(Path.Web.HOME, (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            return template.render(new ModelAndView(model, Path.Template.HOME));
        });

        Functions.redirectTrailingSlash("get", Path.Web.SURVEY);
        get(Path.Web.SURVEY, (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("items", Functions.getSurveyItemList(surveyRepo));
            model.put("items_tooltip", "Cantidad de preguntas");
            return template.render(new ModelAndView(model, Path.Template.SURVEY));
        });

        Functions.redirectTrailingSlash("get", Path.Web.SURVEY_NAME);
        get(Path.Web.SURVEY_NAME, (request, response) -> {
            try {
                Map<String, Object> model = new HashMap<>();
                String name = request.params("name");
                Survey survey = surveyRepo.findByName(name);
                SurveyType type = survey.getType();
                String path = Path.Web.SURVEY_NAME_REPLY.replace(Path.NAME_VAR, name);
                model.put("items", Functions.getSurveyItemDetail(survey));
                model.put("title", "Encuesta: " + Functions.firstLetterCap(name));
                model.put("reply_button", path);
                model.put("survey_type", Functions.firstLetterCap(type.getLocalizedType()));
                model.put("isEvaluative", type == SurveyType.EVALUATION);
                return template.render(new ModelAndView(model, Path.Template.SURVEY_NAME));

            } catch (SurveyRepositoryException e) {
                // no logger system was implemented
                return getError(HttpStatus.NOT_FOUND_404,
                                response,
                                "No se encontró la encuesta buscada",
                                "Intente buscando otra en la lista de encuestas",
                                Path.Web.SURVEY);
            }
        });

        Functions.redirectTrailingSlash("get", Path.Web.SURVEY_NAME_REPLY);
        get(Path.Web.SURVEY_NAME_REPLY, (request, response) -> {
            try {
                Map<String, Object> model = new HashMap<>();
                String name = request.params("name");
                Survey survey = surveyRepo.findByName(name);
                model.put("survey", survey);
                return template.render(new ModelAndView(model, Path.Template.SURVEY_NAME_REPLY));

            } catch (SurveyRepositoryException e) {
                // no logger system was implemented
                return getError(HttpStatus.NOT_FOUND_404,
                                response,
                                "No se encontró la encuesta buscada para responderla",
                                "Intente buscando otra en la lista de encuestas",
                                Path.Web.SURVEY);
            }
        });

        Functions.redirectTrailingSlash("post", Path.Web.SURVEY_NAME_REPLY);
        post(Path.Web.SURVEY_NAME_REPLY, (request, response) -> {
            String name = request.params("name");
            try {
                manager.getTransaction().begin();
                Answer answer = Functions.getAnswerFromBody(surveyRepo, request.body());

                if (!nameCache.isReserved(request.session().id(), answer.getName())) {
                    return getError(HttpStatus.BAD_REQUEST_400,
                                    response,
                                    "El nombre elegido estaba reservado",
                                    "Puede intentar realizarla otra vez",
                                    Path.Web.SURVEY_NAME_REPLY.replace(Path.NAME_VAR, name));
                }

                String nameEncoded = URLEncoder.encode(answer.getName(), UTF8).replace("+", " ");
                answerRepo.save(answer);
                manager.getTransaction().commit();
                response.redirect(Path.Web.ANSWER_NAME.replace(Path.NAME_VAR, nameEncoded));
                nameCache.remove(request.session().id());
                return null;

            } catch (MissingIdException | UnsupportedEncodingException e) {
                // no logger system was implemented
                return getError(HttpStatus.BAD_REQUEST_400,
                                response,
                                "Su respuesta llegó con errores al servidor",
                                "Puede intentar realizarla otra vez",
                                Path.Web.SURVEY_NAME_REPLY.replace(Path.NAME_VAR, name));

            } catch (ServerException e) {
                // no logger system was implemented
                return getError(HttpStatus.NOT_FOUND_404,
                                response,
                                "No se encontró la encuesta que estaba respondiendo",
                                "Intente buscando otra en la lista de encuestas",
                                Path.Web.SURVEY);

            } catch (AnswerRepositoryException e) {
                // no logger system was implemented
                return getError(HttpStatus.BAD_REQUEST_400,
                                response,
                                "Ocurrio un error al guardar su respuesta",
                                "Puede intentar realizarla otra vez",
                                Path.Web.SURVEY_NAME_REPLY.replace(Path.NAME_VAR, name));
            }
        });

        // ---------------------------------------------------------------------

        Functions.redirectTrailingSlash("get", Path.Web.ANSWER);
        get(Path.Web.ANSWER, (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("items", Functions.getAnswerItemList(answerRepo));
            model.put("items_tooltip", "Cantidad de respuestas");
            return template.render(new ModelAndView(model, Path.Template.ANSWER));
        });

        Functions.redirectTrailingSlash("get", Path.Web.ANSWER_NAME);
        get(Path.Web.ANSWER_NAME, (request, response) -> {
            try {
                Map<String, Object> model = new HashMap<>();
                String name = request.params("name");
                Answer answer = answerRepo.findByName(name);
                SurveyType type = answer.getSurvey().getType();
                model.put("items", Functions.getAnswerItemDetail(answer));
                model.put("title", "Respuesta: " + Functions.firstLetterCap(name));
                model.put("author", "Respondió: " + Functions.firstLetterCap(answer.getAuthor()));
                model.put("survey_type", Functions.firstLetterCap(type.getLocalizedType()));
                model.put("isEvaluative", type == SurveyType.EVALUATION);
                return template.render(new ModelAndView(model, Path.Template.ANSWER_NAME));

            } catch (AnswerRepositoryException e) {
                // no logger system was implemented
                return getError(HttpStatus.NOT_FOUND_404,
                                response,
                                "No se encontró la respuesta que buscaba",
                                "Intente buscando otra en la lista de respuestas",
                                Path.Web.ANSWER);
            }
        });

        Functions.redirectTrailingSlash("get", Path.Web.ANSWER_NAME_RESERVE);
        get(Path.Web.ANSWER_NAME_RESERVE, (request, response) -> {
            String name = request.params("name").trim();
            boolean reserved = false;
            if (!answerRepo.existsByName(name)) {
                reserved = nameCache.reserve(request.session().id(), name, true);
            }
            return Functions.processReservation(response, reserved, name);
        });

        Functions.redirectTrailingSlash("post", Path.Web.ANSWER_NAME_RESERVE);
        post(Path.Web.ANSWER_NAME_RESERVE, (request, response) -> {
            String name = request.params("name").trim();
            boolean reserved = nameCache.isReserved(request.session().id(), name);
            return Functions.processReservation(response, reserved);
        });

        // ---------------------------------------------------------------------

        get("*", (request, response) -> {
            return getError(HttpStatus.NOT_FOUND_404,
                            response,
                            "No se encontro el recurso buscado",
                            "Puede volver a la pagina de inicio",
                            Path.Web.HOME);
        });

        internalServerError((request, response) -> {
            return getError(HttpStatus.INTERNAL_SERVER_ERROR_500,
                            response,
                            "Ocurrio un error en el servidor, el desarrollador " +
                                      "ya fue notificado, disculpe las molestias",
                            "Puede volver a la pagina de inicio",
                            Path.Web.HOME);
        });
    }

    private String getError(int status, Response response, String message, String... items) {
        if (manager.getTransaction().isActive()) {
            manager.getTransaction().rollback();
        }
        response.status(status);

        List<ItemAction> actions = new ArrayList<>();
        for (int i = 0; i < items.length; i += 2) {
            actions.add(new ItemAction(items[i], items[i + 1]));
        }

        Map<String, Object> model = new HashMap<>();
        model.put("message", message);
        model.put("actions", actions);

        response.status(HttpStatus.PERMANENT_REDIRECT_308);
        return template.render(new ModelAndView(model, Path.Template.NOT_FOUND));
    }

    public static void main(String[] args) {
        try {
            Server server = new Server();
            server.setRoutes();
        } catch (Exception e) {
            // no logger system was implemented
            e.printStackTrace();
        }
    }
}
