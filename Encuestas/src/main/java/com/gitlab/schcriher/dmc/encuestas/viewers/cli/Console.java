package com.gitlab.schcriher.dmc.encuestas.viewers.cli;

import java.nio.charset.StandardCharsets;
import java.util.Locale;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.domain.Choice;
import com.gitlab.schcriher.dmc.encuestas.domain.Question;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionType;
import com.gitlab.schcriher.dmc.encuestas.domain.Selection;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;

public class Console {

    private static final Pattern FIELDS = Pattern.compile("\\s*,\\s*",
                                                          Pattern.UNICODE_CHARACTER_CLASS);

    private static final Locale LOCALE = Locale.forLanguageTag("es");
    private static final String UTF8 = StandardCharsets.UTF_8.name();

    private Survey survey;
    private Answer answer;

    public Console(Survey survey, Answer answer) {
        this.survey = survey;
        this.answer = answer;
        answer.setSurvey(survey);
    }

    public Answer getAnswer() {
        return answer;
    }

    public void run() {
        Scanner input = new Scanner(System.in, UTF8);

        System.out.print("\n¡Bienvenido! está por contestar la encuesta '");
        System.out.print(survey.getName());
        System.out.print("'\n");

        System.out.print("\nPor favor escriba su nombre: ");
        String author = input.nextLine();
        answer.setAuthor(author);

        System.out.print("\nEncuesta de tipo ");
        System.out.print(survey.getType().getLocalizedType().toLowerCase(LOCALE));

        for (Question question : survey.getQuestions()) {
            ask(input, question);
        }

        input.close();
        System.out.println("\nGracias por responder " + author + ".");
    }

    private void ask(Scanner input, Question question) {

        final int limit = question.getLimitChoices();
        final boolean choicesRequired = question.isChoicesRequired();
        final QuestionType questionType = question.getType();

        final StringBuilder display = new StringBuilder("\n\n");
        display.append("Pregunta de opción ");
        display.append(questionType.getLocalizedType().toLowerCase(LOCALE));
        display.append("\n");

        switch (questionType) {

            case SINGLE:
                display.append("Para responder escriba el número elegido\n");
                break;

            case MULTIPLE:
                display.append("Para responder escriba el/los número/s ");
                display.append("separados con comas, puede dejar vacio\n");
                break;
        }

        display.append("\nPregunta: ");
        display.append(question.getText());
        display.append("\n");

        final int numChoices = question.getNumChoices();
        for (int index = 0; index < numChoices; index++) {
            display.append(index + 1);
            display.append(") ");
            display.append(question.getChoice(index).getText());
            display.append("\n");
        }
        display.append("\nRespuesta: ");
        System.out.print(display.toString());

        Selection selection = new Selection();
        selection.setAnswer(answer);
        selection.setQuestion(question);

        int count = 0;
        while (true) {
            String entered = input.nextLine();
            if (!entered.isEmpty()) {
                for (String selected : FIELDS.split(entered)) {
                    if (count > limit) {
                        System.out.println("Opción ignorada: " + selected);
                    } else {
                        count += addChoiceSelected(question, selection, selected);
                    }
                }
            }
            if (choicesRequired && count == 0) {
                System.out.print("Se requiere una respuesta: ");
            } else {
                break;
            }
        }
        answer.addSelection(selection);
    }

    private static int addChoiceSelected(Question question, Selection selection, String selected) {
        try {
            int index = Integer.parseInt(selected) - 1;
            Choice choice = question.getChoice(index);
            if (selection.contains(choice)) {
                System.out.println("Opción repetida: " + selected);
            } else {
                selection.addChoice(choice);
                return 1;
            }
        } catch (NumberFormatException e) {
            // System interacts with the user via console
            System.out.println("No es un número: " + selected);
        } catch (IndexOutOfBoundsException e) {
            // System interacts with the user via console
            System.out.println("Fuera de rango: " + selected);
        }
        return 0;
    }
}
