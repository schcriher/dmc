package com.gitlab.schcriher.dmc.encuestas.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "selection")
public class Selection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @JsonIgnore
    private Integer id;

    @ManyToOne
    @JsonIgnore
    private Answer answer;

    @ManyToOne
    private Question question;

    @JoinTable(name = "selection_choice",
               inverseJoinColumns = @JoinColumn(name = "choice_id"))
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private List<Choice> choices;

    public Selection() {
        id = null;  // is null to be handled by the database
        choices = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public List<Choice> getChoices() {
        return Collections.unmodifiableList(choices);
    }

    public void setChoices(Iterable<Choice> choices) {
        this.choices.clear();
        for (Choice choice : choices) {
            addChoice(choice);
        }
    }

    public void addChoice(Choice choice) {
        choices.add(choice);
    }

    public boolean contains(Choice choice) {
        return choices.contains(choice);
    }

    @Override
    public String toString() {
        return String.format("Selection [id=%s, answer=%s, question=%s, choices=%d]",
                             id, answer.getName(), question.getText(), choices.size());
    }

    @Override
    public int hashCode() {
        return Objects.hash(choices);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Selection other = (Selection) obj;
        return Objects.equals(choices, other.choices);
    }
}
