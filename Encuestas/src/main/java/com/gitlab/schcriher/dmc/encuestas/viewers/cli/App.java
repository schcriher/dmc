package com.gitlab.schcriher.dmc.encuestas.viewers.cli;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerJdbcRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerJpaRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerJsonRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.AnswerTxtRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyJdbcRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyJpaRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyJsonRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyRepository;
import com.gitlab.schcriher.dmc.encuestas.sources.SurveyTxtRepository;

public class App {

    private Connection connection;
    private EntityManagerFactory factory;
    private EntityManager manager;
    private EntityTransaction transaction;

    private SurveyRepository surveyRepo;
    private AnswerRepository answerRepo;

    public App() throws AppException, SQLException {
        Map<String, String> env = System.getenv();

        String surveyTxtDirectory = env.getOrDefault("ENCUESTAS_SURVEY_TXT_DIRECTORY", "");
        String answerTxtDirectory = env.getOrDefault("ENCUESTAS_ANSWER_TXT_DIRECTORY", "");
        String surveyJsonDirectory = env.getOrDefault("ENCUESTAS_SURVEY_JSON_DIRECTORY", "");
        String answerJsonDirectory = env.getOrDefault("ENCUESTAS_ANSWER_JSON_DIRECTORY", "");
        String uriDatabase = env.getOrDefault("ENCUESTAS_DATABASE_JDBC_URI", "");
        boolean useJPA = Boolean.parseBoolean(env.getOrDefault("ENCUESTAS_DATABASE_JPA_USE", ""));

        if (!surveyTxtDirectory.isEmpty() && !answerTxtDirectory.isEmpty()) {
            File surveyDir = new File(surveyTxtDirectory);
            File answerDir = new File(answerTxtDirectory);
            testDirectory(surveyDir, answerDir);
            surveyRepo = new SurveyTxtRepository(surveyDir);
            answerRepo = new AnswerTxtRepository(answerDir);

        } else if (!surveyJsonDirectory.isEmpty() && !answerJsonDirectory.isEmpty()) {
            File surveyDir = new File(surveyJsonDirectory);
            File answerDir = new File(answerJsonDirectory);
            testDirectory(surveyDir, answerDir);
            surveyRepo = new SurveyJsonRepository(surveyDir);
            answerRepo = new AnswerJsonRepository(answerDir);

        } else if (!uriDatabase.isEmpty()) {
            connection = DriverManager.getConnection(uriDatabase);
            connection.setAutoCommit(false);
            surveyRepo = new SurveyJdbcRepository(connection);
            answerRepo = new AnswerJdbcRepository(connection);

        } else if (useJPA) {
            Map<String, String> cfg = new HashMap<>();
            cfg.put("hibernate.connection.url", env.get("JDBC_DATABASE_URL"));
            cfg.put("hibernate.connection.username", env.get("JDBC_DATABASE_USERNAME"));
            cfg.put("hibernate.connection.password", env.get("JDBC_DATABASE_PASSWORD"));
            factory = Persistence.createEntityManagerFactory("db", cfg);
            manager = factory.createEntityManager();
            transaction = manager.getTransaction();
            surveyRepo = new SurveyJpaRepository(manager);
            answerRepo = new AnswerJpaRepository(manager);

        } else {
            throw new AppException("must set in the environment variables:\n" +
                                   " • For txt:  ENCUESTAS_SURVEY_TXT_DIRECTORY and " +
                                   "ENCUESTAS_ANSWER_TXT_DIRECTORY\n" +
                                   " • For json: ENCUESTAS_SURVEY_JSON_DIRECTORY and " +
                                   "ENCUESTAS_ANSWER_JSON_DIRECTORY\n" +
                                   " • For jdbc: ENCUESTAS_DATABASE_JDBC_URI\n" +
                                   " • For JPA:  ENCUESTAS_DATABASE_JPA_USE\n" +
                                   "             JDBC_DATABASE_URL\n" + 
                                   "             JDBC_DATABASE_USERNAME\n" + 
                                   "             JDBC_DATABASE_PASSWORD\n");
        }
    }

    private void run(String surveyName, String answerName) throws AppException {
        try {
            if (transaction != null) {
                transaction.begin();
            }

            Survey survey = surveyRepo.findByName(surveyName);
            Answer answer = answerRepo.createByName(answerName);
            Console console = new Console(survey, answer);
            console.run();
            answerRepo.save(answer);

            if (transaction != null) {
                transaction.commit();
            }
        } catch (Exception e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
            throw new AppException(e);
        } finally {
            if (manager != null) {
                manager.close();
            }
            if (factory != null) {
                factory.close();
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    // System interacts with the user via console
                    System.err.println(e.getLocalizedMessage());
                }
            }
        }
    }

    private static void testDirectory(File surveyDir, File answerDir) throws AppException {

        if (!surveyDir.isDirectory() && !surveyDir.canRead()) {
            String message = "\"%s\" directory does not exist or cannot be read";
            throw new AppException(String.format(message, surveyDir));
        }
        if (!answerDir.isDirectory() && !answerDir.canWrite()) {
            String message = "\"%s\" directory does not exist or cannot be written";
            throw new AppException(String.format(message, answerDir));
        }
    }

    public static void main(String[] args) {
        final int argsNum = 2;
        try {
            if (args.length != argsNum) {
                throw new AppException("Usage: java App surveyName answerName");
            }
            App app = new App();
            app.run(args[0], args[1]);
        } catch (Exception e) {
            // System interacts with the user via console
            System.err.println("Ocurrio un error con la ejecución\n" + e.getLocalizedMessage());
        }
    }
}
