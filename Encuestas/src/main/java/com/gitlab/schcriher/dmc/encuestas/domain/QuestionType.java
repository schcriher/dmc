package com.gitlab.schcriher.dmc.encuestas.domain;

public enum QuestionType {
    SINGLE("Simple"),       // Hardcoded text for learning purposes
    MULTIPLE("Multiple");   // Hardcoded text for learning purposes

    private String localizedType;

    QuestionType(String localizedType) {
        this.localizedType = localizedType;
    }

    public String getLocalizedType() {
        return localizedType;
    }
}
