package com.gitlab.schcriher.dmc.encuestas.viewers.web;

public final class Path {

    public static final String CHAR_SEP = "/";
    public static final String NAME_VAR = ":name";

    public static final class Web {

        public static final String HOME = "/";
        public static final String SURVEY = "/survey";
        public static final String SURVEY_NAME = join(SURVEY, NAME_VAR);
        public static final String SURVEY_NAME_REPLY = join(SURVEY_NAME, "reply");
        public static final String ANSWER = "/answer";
        public static final String ANSWER_NAME = join(ANSWER, NAME_VAR);
        public static final String ANSWER_NAME_RESERVE = join(ANSWER_NAME, "reserve");
        public static final String NOT_FOUND = "*";

    }

    public static final class Template {

        public static final String HOME = "home";
        public static final String SURVEY = "list";
        public static final String SURVEY_NAME = "detail";
        public static final String SURVEY_NAME_REPLY = "reply";
        public static final String ANSWER = "list";
        public static final String ANSWER_NAME = "detail";
        public static final String NOT_FOUND = "error";
    }

    public static String join(String... strings) {
        return String.join(CHAR_SEP, strings);
    }

}
