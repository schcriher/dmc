package com.gitlab.schcriher.dmc.encuestas.sources;

import java.util.List;
import java.util.Objects;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;

public class AnswerJpaRepository implements AnswerRepository {

    private EntityManager manager;

    public AnswerJpaRepository(EntityManager manager) {
        this.manager = manager;
    }

    @Override
    public boolean existsByName(String name) {
        List<Object[]> data = manager.createNativeQuery("SELECT 1 WHERE EXISTS" +
                                                        " (SELECT FROM answer a" +
                                                        "  WHERE a.name = :name);")
                                     .setParameter("name", name)
                                     .getResultList();
        return !data.isEmpty();
    }

    @Override
    public Answer createByName(String name) throws AnswerRepositoryException {
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Answer> query = builder.createQuery(Answer.class);
            Root<Answer> root = query.from(Answer.class);
            query.select(root).where(builder.equal(root.get("name"), name));
            Answer answer = manager.createQuery(query).getSingleResult();
            if (Objects.equals(name, answer.getName())) {
                throw new AnswerRepositoryException(String.format("'%s' answer already exists",
                                                                  name));
            }
            return answer;
        } catch (NoResultException | NonUniqueResultException e) {
            // Is not strictly necessary to logger this exception
            Answer answer = new Answer();
            answer.setName(name);
            return answer;
        }
    }

    @Override
    public void save(Answer answer) throws AnswerRepositoryException {
        try {
            manager.persist(answer);
        } catch (Exception e) {
            // SQLException, JPA does not throw them explicitly,
            // e.g. when a database constraint is violated
            throw new AnswerRepositoryException("answer already exists", e);
        }
    }

    @Override
    public Answer findByName(String name) throws AnswerRepositoryException {
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Answer> query = builder.createQuery(Answer.class);
            Root<Answer> root = query.from(Answer.class);
            query.select(root).where(builder.equal(root.get("name"), name));
            return manager.createQuery(query).getSingleResult();

        } catch (NoResultException | NonUniqueResultException e) {
            throw new AnswerRepositoryException(String.format("\"%s\" answer not found", name), e);
        }
    }

    @Override
    public List<Answer> getAll() {
        return manager.createQuery("SELECT a FROM answer AS a", Answer.class)
                      .getResultList();
    }

}
