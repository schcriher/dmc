package com.gitlab.schcriher.dmc.encuestas.viewers.web.containers;

public class ItemAction {

    private String message;
    private String url;

    public ItemAction(String message, String url) {
        this.message = message;
        this.url = url;
    }

    public String getMessage() {
        return message;
    }

    public String getUrl() {
        return url;
    }

}
