package com.gitlab.schcriher.dmc.encuestas.viewers.web.containers;

public class ItemList {

    private String name;
    private String url;
    private long quantity;

    public ItemList(String name) {
        this(name, "", 0);
    }

    public ItemList(String name, String url, long quantity) {
        this.name = name;
        this.url = url;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public long getQuantity() {
        return quantity;
    }
}
