package com.gitlab.schcriher.dmc.encuestas.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "survey")
public class Survey {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @JsonIgnore
    private Integer id;

    // Unique because of file storage implementations
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private SurveyType type;

    @ManyToMany(cascade = CascadeType.DETACH)
    @JoinTable(name = "survey_question",
               inverseJoinColumns = @JoinColumn(name = "question_id"))
    private List<Question> questions;

    public Survey() {
        id = null;  // is null to be handled by the database
        questions = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SurveyType getType() {
        return type;
    }

    public void setType(SurveyType type) {
        this.type = type;
    }

    public List<Question> getQuestions() {
        return Collections.unmodifiableList(questions);
    }

    public void setQuestions(Iterable<Question> questions) throws SurveyException {
        this.questions.clear();
        for (Question correct : questions) {
            addQuestion(correct);
        }
    }

    public void addQuestion(Question question) throws SurveyException {
        int correctChoices = question.getCorrectChoices().size();
        switch (type) {

            case STATISTICS:
                if (correctChoices != 0) {
                    throw new SurveyException("statistical questions should not contain correct choices");
                }
                break;

            case EVALUATION:
                if (correctChoices == 0) {
                    throw new SurveyException("evaluation questions must contain at least one correct choice");
                }
                break;
        }
        if (question.getNumChoices() > 1) {
            questions.add(question);
        } else {
            throw new SurveyException("question must contain more than one choice");
        }
    }

    @Override
    public String toString() {
        return String.format("Survey [id=%s, name=%s, type=%s, questions=%d]",
                             id, name, type, questions.size());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, type, questions);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Survey other = (Survey) obj;
        return Objects.equals(name, other.name) &&
               Objects.equals(type, other.type) &&
               Objects.equals(questions, other.questions);
    }

    public Question getQuestionById(int qid) throws SurveyException {
        for (Question question : questions) {
            if (question.getId() == qid) {
                return question;
            }
        }
        throw new SurveyException("survey not found, id: " + qid);
    }
}
