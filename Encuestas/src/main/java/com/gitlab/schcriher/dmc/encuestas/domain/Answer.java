package com.gitlab.schcriher.dmc.encuestas.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "answer")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @JsonIgnore
    private Integer id;

    // Unique because of file storage implementations
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Column(name = "author", nullable = false)
    private String author;

    @Column(name = "datetime", nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime dateTime;

    @ManyToOne
    private Survey survey;

    @OneToMany(mappedBy = "answer",
               cascade = CascadeType.ALL,
               fetch = FetchType.EAGER,
               orphanRemoval = true)
    private List<Selection> selections;

    public Answer() {
        id = null;  // is null to be handled by the database
        dateTime = LocalDateTime.now();
        selections = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime date) {
        this.dateTime = date;
    }

    public List<Selection> getSelections() {
        return Collections.unmodifiableList(selections);
    }

    public void setSelections(Iterable<Selection> selections) {
        this.selections.clear();
        for (Selection selection : selections) {
            addSelection(selection);
        }
    }

    public void addSelection(Selection selection) {
        selections.add(selection);
    }

    @Override
    public String toString() {
        return String.format("Answer [id=%s, name=%s, author=%s, dateTime=%s, survey=%s, selections=%d]",
                             id, name, author, dateTime, survey.getName(), selections.size());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, author, dateTime, survey, selections);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Answer other = (Answer) obj;
        return Objects.equals(name, other.name) &&
               Objects.equals(author, other.author) &&
               Objects.equals(dateTime, other.dateTime) &&
               Objects.equals(survey, other.survey) &&
               Objects.equals(selections, other.selections);
    }
}
