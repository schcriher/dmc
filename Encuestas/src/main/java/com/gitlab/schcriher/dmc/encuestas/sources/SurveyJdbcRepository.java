package com.gitlab.schcriher.dmc.encuestas.sources;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.gitlab.schcriher.dmc.encuestas.domain.Choice;
import com.gitlab.schcriher.dmc.encuestas.domain.Question;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionException;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionType;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyException;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyType;

public class SurveyJdbcRepository implements SurveyRepository {

    private static final String NOT_FOUND = "survey not found in the database";

    private Connection connection;

    public SurveyJdbcRepository(Connection connection) {
        this.connection = connection;
    }

    private ResultSet getResult(String sql, Object... args) throws SurveyRepositoryException {
        try (PreparedStatement statement = connection.prepareStatement(sql)) {
            int index = 1;
            for (Object arg : args) {
                statement.setObject(index, arg);
                index++;
            }
            return statement.executeQuery();
        } catch (SQLException e) {
            throw new SurveyRepositoryException(NOT_FOUND, e);
        }
    }

    @Override
    public Survey findByName(String name) throws SurveyRepositoryException {
        try {
            ResultSet srs = getResult("SELECT s.id AS sid, st.text AS type" +
                                      "  FROM survey AS s" +
                                      "  JOIN survey_type AS st ON s.type_id = st.id" +
                                      " WHERE s.name = ?;",
                                      name);

            srs.next();
            SurveyType surveyType = SurveyType.valueOf(srs.getString("type"));
            Integer sid = Integer.parseInt(srs.getString("sid"));
            srs.close();

            Survey survey = new Survey();
            survey.setId(sid);
            survey.setName(name);
            survey.setType(surveyType);

            ResultSet qrs = getResult("SELECT q.id AS qid, q.text AS question, qt.text AS type" +
                                      "  FROM question AS q" +
                                      "  JOIN question_type AS qt ON qt.id = q.type_id" +
                                      "  JOIN survey_question AS sq ON sq.question_id = q.id" +
                                      "   AND sq.survey_id = ?;",
                                      sid);

            while (qrs.next()) {
                Integer qid = Integer.parseInt(qrs.getString("qid"));
                Question question = new Question();
                question.setId(qid);
                question.setText(qrs.getString("question"));
                question.setType(QuestionType.valueOf(qrs.getString("type")));

                ResultSet crs = getResult("SELECT c.id AS cid, c.text AS choice" +
                                          "  FROM choice AS c" +
                                          "  JOIN question_choice AS qc ON qc.choice_id = c.id" +
                                          "   AND qc.question_id = ?;",
                                          qid);

                while (crs.next()) {
                    Integer cid = Integer.parseInt(crs.getString("cid"));
                    Choice choice = new Choice();
                    choice.setId(cid);
                    choice.setText(crs.getString("choice"));
                    question.addChoice(choice);

                    if (surveyType == SurveyType.EVALUATION) {
                        ResultSet qccrs = getResult("SELECT" +
                                                    "  FROM question_correct_choice AS qcc" +
                                                    " WHERE qcc.question_id = ?" +
                                                    "   AND qcc.choice_id = ?;",
                                                    qid,
                                                    cid);

                        if (qccrs.next()) {
                            question.addCorrectChoice(choice);
                        }
                        qccrs.close();
                    }
                }
                crs.close();
                survey.addQuestion(question);
            }
            qrs.close();
            return survey;
        } catch (SurveyException | QuestionException | SQLException e) {
            throw new SurveyRepositoryException(NOT_FOUND, e);
        }
    }

    @Override
    public List<Survey> getAll() {
        throw new UnsupportedOperationException();
    }
}
