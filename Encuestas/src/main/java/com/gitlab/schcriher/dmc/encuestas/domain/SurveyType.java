package com.gitlab.schcriher.dmc.encuestas.domain;

public enum SurveyType {
    STATISTICS("Estadística"),  // Hardcoded text for learning purposes
    EVALUATION("Evaluatoria");  // Hardcoded text for learning purposes

    private String localizedType;

    SurveyType(String localizedType) {
        this.localizedType = localizedType;
    }

    public String getLocalizedType() {
        return localizedType;
    }
}
