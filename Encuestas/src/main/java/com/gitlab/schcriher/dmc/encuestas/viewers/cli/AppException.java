package com.gitlab.schcriher.dmc.encuestas.viewers.cli;

import com.gitlab.schcriher.dmc.encuestas.shared.AbstractBaseException;

public class AppException extends AbstractBaseException {

    private static final long serialVersionUID = 1L;

    public AppException() {
        this(null, null);
    }

    public AppException(String detail) {
        super(detail, null);
    }

    public AppException(Throwable cause) {
        super(null, cause);
    }

    public AppException(String detail, Throwable cause) {
        super(detail, cause);
    }
}
