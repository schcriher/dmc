package com.gitlab.schcriher.dmc.encuestas.sources;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.gitlab.schcriher.dmc.encuestas.domain.Survey;

public class SurveyJpaRepository implements SurveyRepository {

    private EntityManager manager;

    public SurveyJpaRepository(EntityManager manager) {
        this.manager = manager;
    }

    @Override
    public Survey findByName(String name) throws SurveyRepositoryException {
        try {
            CriteriaBuilder builder = manager.getCriteriaBuilder();
            CriteriaQuery<Survey> query = builder.createQuery(Survey.class);
            Root<Survey> root = query.from(Survey.class);
            query.select(root).where(builder.equal(root.get("name"), name));
            return manager.createQuery(query).getSingleResult();

        } catch (NoResultException | NonUniqueResultException e) {
            throw new SurveyRepositoryException("survey not found in the database", e);
        }
    }

    @Override
    public List<Survey> getAll() {
        return manager.createQuery("SELECT s FROM survey AS s", Survey.class)
                      .getResultList();
    }
}
