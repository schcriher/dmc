package com.gitlab.schcriher.dmc.encuestas.domain;

import com.gitlab.schcriher.dmc.encuestas.shared.AbstractBaseException;

public class SurveyException extends AbstractBaseException {

    private static final long serialVersionUID = 1L;

    public SurveyException() {
        this(null, null);
    }

    public SurveyException(String detail) {
        super(detail, null);
    }

    public SurveyException(Throwable cause) {
        super(null, cause);
    }

    public SurveyException(String detail, Throwable cause) {
        super(detail, cause);
    }
}
