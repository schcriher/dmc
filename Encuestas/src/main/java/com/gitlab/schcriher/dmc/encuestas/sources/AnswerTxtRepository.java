package com.gitlab.schcriher.dmc.encuestas.sources;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Locale;

import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.domain.Question;
import com.gitlab.schcriher.dmc.encuestas.domain.Selection;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyType;

public class AnswerTxtRepository implements AnswerRepository {

    private static final Locale LOCALE = Locale.forLanguageTag("es");
    private final File directory;

    public AnswerTxtRepository(File directory) {
        this.directory = directory;
    }

    private File getFile(String name) {
        return new File(directory.getAbsoluteFile(), name + ".txt");
    }

    @Override
    public boolean existsByName(String name) {
        return getFile(name).exists();
    }

    @Override
    public Answer createByName(String name) throws AnswerRepositoryException {
        if (getFile(name).exists()) {
            throw new AnswerRepositoryException("answer already exists");
        }
        Answer answer = new Answer();
        answer.setName(name);
        return answer;
    }

    private static Selection getSelectionFromQuestion(Question question,
                                                      List<Selection> selections) throws AnswerRepositoryException {
        for (Selection selection : selections) {
            if (selection.getQuestion().equals(question)) {
                return selection;
            }
        }
        String message = "question \"%s\" is not found in the selections";
        throw new AnswerRepositoryException(String.format(message, question.getText()));
    }

    @Override
    public void save(Answer answer) throws AnswerRepositoryException {

        List<Selection> selections = answer.getSelections();
        Survey survey = answer.getSurvey();
        SurveyType surveyType = survey.getType();

        StringBuilder output = new StringBuilder();
        output.append("Encuesta: ");
        output.append(survey.getName());
        output.append("\n");
        output.append("Del tipo: ");
        output.append(surveyType.getLocalizedType().toLowerCase(LOCALE));
        output.append("\n\n");
        output.append("Respondido por ");
        output.append(answer.getAuthor());
        output.append(String.format(Locale.getDefault(),
                                    " el %1$td/%1$tm/%1$tY a las %1$tT.",
                                    answer.getDateTime()));
        output.append("\n");

        for (Question question : survey.getQuestions()) {
            Selection selection = getSelectionFromQuestion(question, selections);

            output.append("\nPregunta: ");
            output.append(question.getText());
            output.append("\n");
            output.append("Respuesta:\n");

            question.getChoices().forEach(choice -> {
                output.append("  ");
                output.append(choice.getText());
                output.append("  [");
                if (selection.contains(choice)) {
                    output.append("elegido");
                } else {
                    output.append("no elegido");
                }
                if (surveyType == SurveyType.EVALUATION) {
                    output.append(question.isCorrectChoice(choice) ? ", correcto" : ", incorrecto");
                }
                output.append("]\n");
            });
        }
        try (FileOutputStream fos = new FileOutputStream(getFile(answer.getName()));
             OutputStreamWriter osr = new OutputStreamWriter(fos, StandardCharsets.UTF_8);
             BufferedWriter writer = new BufferedWriter(osr)) {
            writer.write(output.toString());
        } catch (IOException e) {
            throw new AnswerRepositoryException(e);
        }
    }

    @Override
    public List<Answer> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Answer findByName(String name) {
        throw new UnsupportedOperationException();
    }
}
