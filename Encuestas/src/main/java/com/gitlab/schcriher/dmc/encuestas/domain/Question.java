package com.gitlab.schcriher.dmc.encuestas.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "question")
public class Question {

    private static final String QUESTION_REGEX = "[^¿\\?]*¿[^¿\\\\?]+\\?[^¿\\\\?]*";
    private static final Pattern QUESTION_PATTERN = Pattern.compile(QUESTION_REGEX);

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    @JsonIgnore
    private Integer id;

    @Column(name = "text", nullable = false, unique = true)
    private String text;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false)
    private QuestionType type;

    @JoinTable(name = "question_choice",
               inverseJoinColumns = @JoinColumn(name = "choice_id"))
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    private List<Choice> choices;

    @JoinTable(name = "question_correct_choice",
               inverseJoinColumns = @JoinColumn(name = "choice_id"))
    @ManyToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    private List<Choice> correctChoices;

    public Question() {
        id = null;  // is null to be handled by the database
        choices = new ArrayList<>();
        correctChoices = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) throws QuestionException {
        if (QUESTION_PATTERN.matcher(text).matches()) {
            this.text = text;
        } else {
            throw new QuestionException("text should contain one pair of question marks");
        }
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public Choice getChoice(int index) throws IndexOutOfBoundsException {
        return choices.get(index);
    }

    public Choice getChoiceById(int cid) throws QuestionException {
        for (Choice choice : choices) {
            if (choice.getId() == cid) {
                return choice;
            }
        }
        throw new QuestionException("choice not found, id: " + cid);
    }

    public List<Choice> getChoices() {
        return Collections.unmodifiableList(choices);
    }

    public void setChoices(Iterable<Choice> choices) {
        this.choices.clear();
        for (Choice choice : choices) {
            addChoice(choice);
        }
    }

    public void addChoice(Choice choice) {
        choices.add(choice);
    }

    public List<Choice> getCorrectChoices() {
        return Collections.unmodifiableList(correctChoices);
    }

    public void setCorrectChoices(Iterable<Choice> corrects) throws QuestionException {
        this.correctChoices.clear();
        for (Choice correct : corrects) {
            addCorrectChoice(correct);
        }
    }

    public void addCorrectChoice(Choice choice) throws QuestionException {
        if (type == QuestionType.SINGLE && !correctChoices.isEmpty()) {
            throw new QuestionException("single type of choice only admits one correct");
        }
        if (!choices.contains(choice)) {
            throw new QuestionException("To add a correct choice, it must be included in the choices");
        }
        correctChoices.add(choice);
    }

    public boolean isCorrectChoice(Choice choice) {
        return correctChoices.contains(choice);
    }

    public int getLimitChoices() {
        if (type == QuestionType.SINGLE) {
            return 1;
        }
        return choices.size();
    }

    public boolean isChoicesRequired() {
        return type == QuestionType.SINGLE || !correctChoices.isEmpty();
    }

    public int getNumChoices() {
        return choices.size();
    }

    @Override
    public String toString() {
        return String.format("Question [id=%s, text=%s, type=%s, choices=%d, correctChoices=%d]",
                             id, text, type, choices.size(), correctChoices.size());
    }

    @Override
    public int hashCode() {
        return Objects.hash(text, type, choices, correctChoices);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final Question other = (Question) obj;
        return Objects.equals(text, other.text) &&
               Objects.equals(type, other.type) &&
               Objects.equals(choices, other.choices) &&
               Objects.equals(correctChoices, other.correctChoices);
    }
}
