package com.gitlab.schcriher.dmc.encuestas.sources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;

import com.gitlab.schcriher.dmc.encuestas.domain.Choice;
import com.gitlab.schcriher.dmc.encuestas.domain.Question;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionException;
import com.gitlab.schcriher.dmc.encuestas.domain.QuestionType;
import com.gitlab.schcriher.dmc.encuestas.domain.Survey;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyException;
import com.gitlab.schcriher.dmc.encuestas.domain.SurveyType;

public class SurveyTxtRepository implements SurveyRepository {

    private final File directory;

    public SurveyTxtRepository(File directory) {
        this.directory = directory;
    }

    private static Question getQuestion(BufferedReader buffer) throws SurveyRepositoryException {
        try {
            String line;
            line = buffer.readLine();
            if (line != null) {
                Question question = new Question();
                question.setText(line);
                question.setType(QuestionType.valueOf(buffer.readLine()));

                int index = 0;
                while ((line = buffer.readLine()) != null && !"".equals(line)) {
                    String[] data = line.split("\\|");
                    Choice choice = new Choice();
                    choice.setText(data[1]);
                    question.addChoice(choice);
                    if ("1".equals(data[0])) {
                        question.addCorrectChoice(choice);
                    }
                    index++;
                }
                return line != null || index > 0 ? question : null;
            }
            return null;
        } catch (QuestionException e) {
            throw new SurveyRepositoryException("badly formatted question", e);
        } catch (IOException e) {
            throw new SurveyRepositoryException(e);
        }
    }

    private static Survey getSurveyFromFile(File file) throws SurveyRepositoryException {
        try (FileInputStream fis = new FileInputStream(file);
             InputStreamReader isr = new InputStreamReader(fis, StandardCharsets.UTF_8);
             BufferedReader reader = new BufferedReader(isr)) {

            Survey survey = new Survey();
            survey.setType(SurveyType.valueOf(reader.readLine()));

            // File structure has an empty line after the survey type
            reader.readLine();

            Question question;
            while ((question = getQuestion(reader)) != null) {
                survey.addQuestion(question);
            }
            return survey;
        } catch (SurveyException e) {
            throw new SurveyRepositoryException("badly formatted survey", e);
        } catch (SurveyRepositoryException | IOException e) {
            throw new SurveyRepositoryException(e);
        }
    }

    @Override
    public Survey findByName(String name) throws SurveyRepositoryException {
        File file = new File(directory.getAbsoluteFile(), name + ".txt");
        if (!file.exists()) {
            throw new SurveyRepositoryException(String.format("Survey '%s' does not exist", name));
        }
        Survey survey = getSurveyFromFile(file);
        survey.setName(name);
        return survey;
    }

    @Override
    public List<Survey> getAll() {
        throw new UnsupportedOperationException();
    }
}
