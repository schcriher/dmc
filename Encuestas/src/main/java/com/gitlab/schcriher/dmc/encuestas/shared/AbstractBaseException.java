package com.gitlab.schcriher.dmc.encuestas.shared;

public abstract class AbstractBaseException extends Exception {

    private static final long serialVersionUID = 1L;
    private final String detail;

    protected AbstractBaseException() {
        this(null, null);
    }

    protected AbstractBaseException(String detail) {
        this(detail, null);
    }

    protected AbstractBaseException(Throwable cause) {
        this(null, cause);
    }

    protected AbstractBaseException(String detail, Throwable cause) {
        super(cause);
        this.detail = detail;
    }

    @Override
    public String getLocalizedMessage() {
        String messageA = getStringNoNull(super.getLocalizedMessage());
        String messageB = getStringNoNull(detail);
        String sep = messageA.isEmpty() || messageB.isEmpty() ? "" : ": ";
        return messageA + sep + messageB;
    }

    private static String getStringNoNull(String text) {
        return text != null ? text : "";
    }
}
