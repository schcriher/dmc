package com.gitlab.schcriher.dmc.encuestas.sources;

import java.io.File;
import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.gitlab.schcriher.dmc.encuestas.domain.Answer;
import com.gitlab.schcriher.dmc.encuestas.shared.SurveyJsonSerializer;

public class AnswerJsonRepository implements AnswerRepository {

    private final File directory;
    private final ObjectMapper mapper;

    public AnswerJsonRepository(File directory) {
        this.directory = directory;
        mapper = JsonMapper.builder().addModule(new JavaTimeModule()).build();
        mapper.registerModule(SurveyJsonSerializer.getModule());
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
    }

    private File getFile(String name) {
        return new File(directory.getAbsoluteFile(), name + ".json");
    }

    @Override
    public boolean existsByName(String name) {
        return getFile(name).exists();
    }

    @Override
    public Answer createByName(String name) throws AnswerRepositoryException {
        if (getFile(name).exists()) {
            throw new AnswerRepositoryException("answer already exists");
        }
        Answer answer = new Answer();
        answer.setName(name);
        return answer;
    }

    @Override
    public void save(Answer answer) throws AnswerRepositoryException {
        try {
            mapper.writeValue(getFile(answer.getName()), answer);
        } catch (IOException e) {
            throw new AnswerRepositoryException(e);
        }
    }

    @Override
    public List<Answer> getAll() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Answer findByName(String name) {
        throw new UnsupportedOperationException();
    }
}
