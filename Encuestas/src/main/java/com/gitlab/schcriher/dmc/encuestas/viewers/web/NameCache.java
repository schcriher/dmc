package com.gitlab.schcriher.dmc.encuestas.viewers.web;

import java.time.Instant;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NameCache {

    public static final int MILLISECOND_RESERVE_REFRESH = 5_000;
    public static final int MILLISECOND_RESERVE_AGE = 3 * MILLISECOND_RESERVE_REFRESH;

    private final Map<String, String> names;
    private final Map<String, Long> times;

    public NameCache() {
        names = new HashMap<>();
        times = new HashMap<>();
    }

    public boolean isReserved(String user, String name) {
        return reserve(user, name, false);
    }

    public boolean reserve(String user, String name, boolean reserve) {
        final long time = Instant.now().toEpochMilli();
        final long limit = time - MILLISECOND_RESERVE_AGE;

        String storedName = names.get(user);  // if does not exist return null

        if (name.equals(storedName)) {
            if (reserve || times.get(user) > limit) {
                times.put(user, time);
                return true;
            }
        } else {
            boolean reserved = names.containsValue(name);
            if (reserved) {
                reserved = nameStillActive(name, limit);
            }
            if (reserve && !reserved) {
                names.put(user, name);
                times.put(user, time);
                return true;
            }
        }
        names.remove(user);
        times.remove(user);
        return false;
    }

    private boolean nameStillActive(final String name, final long limit) {
        Iterator<Map.Entry<String, String>> entries = names.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<String, String> entry = entries.next();
            if (name.equals(entry.getValue())) {
                if (times.get(entry.getKey()) < limit) {
                    entries.remove();
                    return false;
                }
                break;
            }
        }
        return true;
    }

    public void remove(String user) {
        names.remove(user);
        times.remove(user);
    }
}
