-- $ psql -d encuestas -U cpci --password < run_jdbc.sql

BEGIN;

--

DO $$ DECLARE
    r RECORD;
BEGIN
    FOR r IN (SELECT tablename FROM pg_tables WHERE schemaname = current_schema())
    LOOP
        EXECUTE format('DROP TABLE IF EXISTS %I CASCADE', quote_ident (r.tablename));
    END LOOP;
END $$;

--

CREATE TABLE survey_type (
    id serial NOT NULL,
    "text" varchar NOT NULL,

    CONSTRAINT survey_type_id_pk PRIMARY KEY (id),
    CONSTRAINT survey_type_text_un UNIQUE (text)
);

CREATE TABLE survey (
    id serial NOT NULL,
    "name" varchar NOT NULL,
    type_id int NOT NULL,

    CONSTRAINT survey_id_pk PRIMARY KEY (id),
    CONSTRAINT survey_name_un UNIQUE (name),
    CONSTRAINT survey_type_id_fk FOREIGN KEY (type_id) REFERENCES survey_type (id)
);

CREATE TABLE question_type (
    id serial NOT NULL,
    "text" varchar NOT NULL,

    CONSTRAINT question_type_id_pk PRIMARY KEY (id),
    CONSTRAINT question_type_text_un UNIQUE (text)
);

CREATE TABLE question (
    id serial NOT NULL,
    "text" varchar NOT NULL,
    type_id int NOT NULL,

    CONSTRAINT question_id_pk PRIMARY KEY (id),
    CONSTRAINT question_type_id_fk FOREIGN KEY (type_id) REFERENCES question_type (id)
);

CREATE TABLE choice (
    id serial NOT NULL,
    "text" varchar NOT NULL,

    CONSTRAINT choice_id_pk PRIMARY KEY (id),
    CONSTRAINT choice_text_un UNIQUE (text)
);

CREATE TABLE survey_question (
    survey_id int NOT NULL,
    question_id int NOT NULL,

    CONSTRAINT survey_question_survey_id_fk FOREIGN KEY (survey_id) REFERENCES survey (id),
    CONSTRAINT survey_question_question_id_fk FOREIGN KEY (question_id) REFERENCES question (id)
);

CREATE TABLE question_choice (
    question_id int NOT NULL,
    choice_id int NOT NULL,

    CONSTRAINT question_choice_question_id_fk FOREIGN KEY (question_id) REFERENCES question (id),
    CONSTRAINT question_choice_choice_id_fk FOREIGN KEY (choice_id) REFERENCES choice (id)
);

CREATE TABLE question_correct_choice (
    question_id int NOT NULL,
    choice_id int NOT NULL,

    CONSTRAINT question_correct_choice_question_id_fk FOREIGN KEY (question_id) REFERENCES question (id),
    CONSTRAINT question_correct_choice_choice_id_fk FOREIGN KEY (choice_id) REFERENCES choice (id)
);

--

CREATE TABLE answer (
    id serial NOT NULL,
    "name" varchar NOT NULL,
    author varchar NOT NULL,
    datetime timestamp NOT NULL,
    survey_id int NOT NULL,

    CONSTRAINT answer_id_pk PRIMARY KEY (id),
    CONSTRAINT answer_name_un UNIQUE (name),
    CONSTRAINT answer_survey_id_fk FOREIGN KEY (survey_id) REFERENCES survey (id)
);

CREATE TABLE selection (
    id serial NOT NULL,
    answer_id int NOT NULL,
    question_id int NOT NULL,

    CONSTRAINT selection_id_pk PRIMARY KEY (id),
    CONSTRAINT selection_answer_id_fk FOREIGN KEY (answer_id) REFERENCES answer (id),
    CONSTRAINT selection_question_id_fk FOREIGN KEY (question_id) REFERENCES question (id)
);

CREATE TABLE selection_choice (
    id serial NOT NULL,
    selection_id int NOT NULL,
    choice_id int NOT NULL,

    CONSTRAINT selection_choice_id_pk PRIMARY KEY (id),
    CONSTRAINT selection_choice_selection_id_fk FOREIGN KEY (selection_id) REFERENCES selection (id),
    CONSTRAINT selection_choice_choice_id_fk FOREIGN KEY (choice_id) REFERENCES choice (id)
);

--

INSERT INTO survey_type (id, text) VALUES (0, 'STATISTICS');
INSERT INTO survey_type (id, text) VALUES (1, 'EVALUATION');

SELECT pg_catalog.setval('survey_type_id_seq', 2, true);

INSERT INTO question_type (id, text) VALUES (0, 'SINGLE');
INSERT INTO question_type (id, text) VALUES (1, 'MULTIPLE');

SELECT pg_catalog.setval('question_type_id_seq', 2, true);

INSERT INTO choice (id, text) VALUES (0, 'Verduras');
INSERT INTO choice (id, text) VALUES (1, 'Frutas');
INSERT INTO choice (id, text) VALUES (2, 'Carne');
INSERT INTO choice (id, text) VALUES (3, 'Pescado y marisco');
INSERT INTO choice (id, text) VALUES (4, 'Productos lácteos');
INSERT INTO choice (id, text) VALUES (5, 'Semillas y frutos secos');
INSERT INTO choice (id, text) VALUES (6, 'Aceites y grasas');
INSERT INTO choice (id, text) VALUES (7, 'Cereales');
INSERT INTO choice (id, text) VALUES (8, 'Tubérculos y Legumbres');
INSERT INTO choice (id, text) VALUES (9, 'HTML');
INSERT INTO choice (id, text) VALUES (10, 'C++');
INSERT INTO choice (id, text) VALUES (11, 'CSS');
INSERT INTO choice (id, text) VALUES (12, 'Java');
INSERT INTO choice (id, text) VALUES (13, 'Python');
INSERT INTO choice (id, text) VALUES (14, 'LaTeX');
INSERT INTO choice (id, text) VALUES (15, 'lanzar un error');
INSERT INTO choice (id, text) VALUES (16, 'probar una condición');
INSERT INTO choice (id, text) VALUES (17, 'imprimir por consola un mensaje');
INSERT INTO choice (id, text) VALUES (18, 'ASM');
INSERT INTO choice (id, text) VALUES (19, 'BASIC');
INSERT INTO choice (id, text) VALUES (20, 'C#');
INSERT INTO choice (id, text) VALUES (21, 'C/C++');
INSERT INTO choice (id, text) VALUES (22, 'Cobol');
INSERT INTO choice (id, text) VALUES (23, 'Fortran');
INSERT INTO choice (id, text) VALUES (24, 'JavaScript');
INSERT INTO choice (id, text) VALUES (25, 'Pascal');
INSERT INTO choice (id, text) VALUES (26, 'PHP');
INSERT INTO choice (id, text) VALUES (27, 'Smalltalk');
INSERT INTO choice (id, text) VALUES (28, 'Verterlos a los rios, ya que estos tienen una capacidad purificadora');
INSERT INTO choice (id, text) VALUES (29, 'Tratarlo en planta y luego disponerlos de forma adecuada');
INSERT INTO choice (id, text) VALUES (30, 'Llamar a la policia, ellos sabrán que hacer');
INSERT INTO choice (id, text) VALUES (31, 'Gasolina');
INSERT INTO choice (id, text) VALUES (32, 'Fotovoltaica');
INSERT INTO choice (id, text) VALUES (33, 'Eólica');
INSERT INTO choice (id, text) VALUES (34, 'Plantas de carbón');
INSERT INTO choice (id, text) VALUES (35, 'Diesel');

SELECT pg_catalog.setval('choice_id_seq', 36, true);

--

INSERT INTO question (id, text, type_id) VALUES (0, '¿Cuál de estos alimentos sueles consumir?', 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 2);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 3);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 4);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 5);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 6);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 7);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 8);

INSERT INTO question (id, text, type_id) VALUES (1, '¿Cuál de estos alimentos elegirías hoy?', 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 2);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 3);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 4);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 5);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 6);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 7);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 8);

INSERT INTO question (id, text, type_id) VALUES (8, '¿Qué alimentos no te gustan?', 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 2);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 3);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 4);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 5);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 6);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 7);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 8);

INSERT INTO survey (id, name, type_id) VALUES (0, 'alimentos', 0);
INSERT INTO survey_question (survey_id, question_id) VALUES (0, 0);
INSERT INTO survey_question (survey_id, question_id) VALUES (0, 1);
INSERT INTO survey_question (survey_id, question_id) VALUES (0, 8);

--

INSERT INTO question (id, text, type_id) VALUES (2, '¿Cuáles de los siguientes lenguajes son de programación?', 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 9);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 10);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 11);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 12);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 13);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 14);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (2, 10);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (2, 12);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (2, 13);

INSERT INTO question (id, text, type_id) VALUES (3, 'Un ''if'' ¿sirve para...?', 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (3, 15);
INSERT INTO question_choice (question_id, choice_id) VALUES (3, 16);
INSERT INTO question_choice (question_id, choice_id) VALUES (3, 17);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (3, 16);

INSERT INTO survey (id, name, type_id) VALUES (1, 'trivia de java', 1);
INSERT INTO survey_question (survey_id, question_id) VALUES (1, 2);
INSERT INTO survey_question (survey_id, question_id) VALUES (1, 3);

--

INSERT INTO question (id, text, type_id) VALUES (4, '¿Qué lenguaje de programación conoces?', 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 12);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 13);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 18);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 19);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 20);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 21);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 22);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 23);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 24);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 25);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 26);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 27);

INSERT INTO question (id, text, type_id) VALUES (5, '¿Qué lenguaje de programación prefieres?', 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 12);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 13);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 18);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 19);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 20);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 21);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 22);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 23);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 24);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 25);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 26);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 27);

INSERT INTO survey (id, name, type_id) VALUES (2, 'lenguajes de programación', 0);
INSERT INTO survey_question (survey_id, question_id) VALUES (2, 4);
INSERT INTO survey_question (survey_id, question_id) VALUES (2, 5);

--

INSERT INTO question (id, text, type_id) VALUES (6, 'Con los desechos de una industria ¿qué se debería realizar?', 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (6, 28);
INSERT INTO question_choice (question_id, choice_id) VALUES (6, 29);
INSERT INTO question_choice (question_id, choice_id) VALUES (6, 30);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (6, 29);

INSERT INTO question (id, text, type_id) VALUES (7, '¿Cuáles de estas energías son consideradas limpias?', 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 31);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 32);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 33);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 34);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 35);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (7, 32);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (7, 33);

INSERT INTO survey (id, name, type_id) VALUES (3, 'medio ambiente', 1);
INSERT INTO survey_question (survey_id, question_id) VALUES (3, 6);
INSERT INTO survey_question (survey_id, question_id) VALUES (3, 7);

SELECT pg_catalog.setval('question_id_seq', 9, true);
SELECT pg_catalog.setval('survey_id_seq', 4, true);

--

COMMIT;
