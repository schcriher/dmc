STATISTICS

¿Cuál de estos alimentos sueles consumir?
MULTIPLE
0|Verduras
0|Frutas
0|Carne
0|Pescado y marisco
0|Productos lácteos
0|Semillas y frutos secos
0|Aceites y grasas
0|Cereales
0|Tubérculos y Legumbres

¿Cuál de estos alimentos elegirías hoy?
SINGLE
0|Verduras
0|Frutas
0|Carne
0|Pescado y marisco
0|Productos lácteos
0|Semillas y frutos secos
0|Aceites y grasas
0|Cereales
0|Tubérculos y Legumbres

¿Qué alimentos no te gustan?
MULTIPLE
0|Verduras
0|Frutas
0|Carne
0|Pescado y marisco
0|Productos lácteos
0|Semillas y frutos secos
0|Aceites y grasas
0|Cereales
0|Tubérculos y Legumbres
