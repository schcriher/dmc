EVALUATION

Con los desechos de una industria ¿qué se debería realizar?
SINGLE
0|Verterlos a los rios, ya que estos tienen una capacidad purificadora
1|Tratarlo en planta y luego disponerlos de forma adecuada
0|Llamar a la policia, ellos sabrán que hacer

¿Cuáles de estas energías son consideradas limpias?
MULTIPLE
0|Gasolina
1|Fotovoltaica
1|Eólica
0|Plantas de carbón
0|Diesel
