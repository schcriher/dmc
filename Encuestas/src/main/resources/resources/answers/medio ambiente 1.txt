Encuesta: medio ambiente
Del tipo: evaluatoria

Respondido por Leonardo el 08/03/2021 a las 14:03:19.

Pregunta: Con los desechos de una industria ¿qué se debería realizar?
Respuesta:
  Verterlos a los rios, ya que estos tienen una capacidad purificadora  [no elegido, incorrecto]
  Tratarlo en planta y luego disponerlos de forma adecuada  [no elegido, correcto]
  Llamar a la policia, ellos sabrán que hacer  [elegido, incorrecto]

Pregunta: ¿Cuáles de estas energías son consideradas limpias?
Respuesta:
  Gasolina  [no elegido, incorrecto]
  Fotovoltaica  [elegido, correcto]
  Eólica  [elegido, correcto]
  Plantas de carbón  [no elegido, incorrecto]
  Diesel  [no elegido, incorrecto]
