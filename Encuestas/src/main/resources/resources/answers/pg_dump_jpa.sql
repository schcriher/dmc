--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2 (Debian 13.2-1)
-- Dumped by pg_dump version 13.2 (Debian 13.2-1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: answer; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.answer (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    datetime timestamp without time zone NOT NULL,
    survey_id integer
);


ALTER TABLE public.answer OWNER TO cpci;

--
-- Name: answer_id_seq; Type: SEQUENCE; Schema: public; Owner: cpci
--

CREATE SEQUENCE public.answer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.answer_id_seq OWNER TO cpci;

--
-- Name: answer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cpci
--

ALTER SEQUENCE public.answer_id_seq OWNED BY public.answer.id;


--
-- Name: choice; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.choice (
    id integer NOT NULL,
    text character varying(255) NOT NULL
);


ALTER TABLE public.choice OWNER TO cpci;

--
-- Name: choice_id_seq; Type: SEQUENCE; Schema: public; Owner: cpci
--

CREATE SEQUENCE public.choice_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.choice_id_seq OWNER TO cpci;

--
-- Name: choice_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cpci
--

ALTER SEQUENCE public.choice_id_seq OWNED BY public.choice.id;


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: cpci
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO cpci;

--
-- Name: question; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.question (
    id integer NOT NULL,
    text character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.question OWNER TO cpci;

--
-- Name: question_choice; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.question_choice (
    question_id integer NOT NULL,
    choice_id integer NOT NULL
);


ALTER TABLE public.question_choice OWNER TO cpci;

--
-- Name: question_correct_choice; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.question_correct_choice (
    question_id integer NOT NULL,
    choice_id integer NOT NULL
);


ALTER TABLE public.question_correct_choice OWNER TO cpci;

--
-- Name: question_id_seq; Type: SEQUENCE; Schema: public; Owner: cpci
--

CREATE SEQUENCE public.question_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.question_id_seq OWNER TO cpci;

--
-- Name: question_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cpci
--

ALTER SEQUENCE public.question_id_seq OWNED BY public.question.id;


--
-- Name: selection; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.selection (
    id integer NOT NULL,
    answer_id integer,
    question_id integer
);


ALTER TABLE public.selection OWNER TO cpci;

--
-- Name: selection_choice; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.selection_choice (
    selection_id integer NOT NULL,
    choice_id integer NOT NULL
);


ALTER TABLE public.selection_choice OWNER TO cpci;

--
-- Name: selection_id_seq; Type: SEQUENCE; Schema: public; Owner: cpci
--

CREATE SEQUENCE public.selection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.selection_id_seq OWNER TO cpci;

--
-- Name: selection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cpci
--

ALTER SEQUENCE public.selection_id_seq OWNED BY public.selection.id;


--
-- Name: survey; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.survey (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255) NOT NULL
);


ALTER TABLE public.survey OWNER TO cpci;

--
-- Name: survey_id_seq; Type: SEQUENCE; Schema: public; Owner: cpci
--

CREATE SEQUENCE public.survey_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.survey_id_seq OWNER TO cpci;

--
-- Name: survey_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: cpci
--

ALTER SEQUENCE public.survey_id_seq OWNED BY public.survey.id;


--
-- Name: survey_question; Type: TABLE; Schema: public; Owner: cpci
--

CREATE TABLE public.survey_question (
    survey_id integer NOT NULL,
    question_id integer NOT NULL
);


ALTER TABLE public.survey_question OWNER TO cpci;

--
-- Name: answer id; Type: DEFAULT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.answer ALTER COLUMN id SET DEFAULT nextval('public.answer_id_seq'::regclass);


--
-- Name: choice id; Type: DEFAULT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.choice ALTER COLUMN id SET DEFAULT nextval('public.choice_id_seq'::regclass);


--
-- Name: question id; Type: DEFAULT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question ALTER COLUMN id SET DEFAULT nextval('public.question_id_seq'::regclass);


--
-- Name: selection id; Type: DEFAULT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.selection ALTER COLUMN id SET DEFAULT nextval('public.selection_id_seq'::regclass);


--
-- Name: survey id; Type: DEFAULT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.survey ALTER COLUMN id SET DEFAULT nextval('public.survey_id_seq'::regclass);


--
-- Data for Name: answer; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.answer (id, name, author, datetime, survey_id) FROM stdin;
1	alimentos 1	Leonardo	2021-03-08 14:39:50.929	0
2	trivia de java 1	Leonardo	2021-03-08 14:40:29.206	1
3	lenguajes de programación 1	Leonardo	2021-03-08 14:41:02.182	2
4	medio ambiente 1	Leonardo	2021-03-08 14:41:42.508	3
\.


--
-- Data for Name: choice; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.choice (id, text) FROM stdin;
0	Verduras
1	Frutas
2	Carne
3	Pescado y marisco
4	Productos lácteos
5	Semillas y frutos secos
6	Aceites y grasas
7	Cereales
8	Tubérculos y Legumbres
9	HTML
10	C++
11	CSS
12	Java
13	Python
14	LaTeX
15	lanzar un error
16	probar una condición
17	imprimir por consola un mensaje
18	ASM
19	BASIC
20	C#
21	C/C++
22	Cobol
23	Fortran
24	JavaScript
25	Pascal
26	PHP
27	Smalltalk
28	Verterlos a los rios, ya que estos tienen una capacidad purificadora
29	Tratarlo en planta y luego disponerlos de forma adecuada
30	Llamar a la policia, ellos sabrán que hacer
31	Gasolina
32	Fotovoltaica
33	Eólica
34	Plantas de carbón
35	Diesel
\.


--
-- Data for Name: question; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.question (id, text, type) FROM stdin;
0	¿Cuál de estos alimentos sueles consumir?	MULTIPLE
1	¿Cuál de estos alimentos elegirías hoy?	SINGLE
8	¿Qué alimentos no te gustan?	MULTIPLE
2	¿Cuáles de los siguientes lenguajes son de programación?	MULTIPLE
3	Un 'if' ¿sirve para...?	SINGLE
4	¿Qué lenguaje de programación conoces?	MULTIPLE
5	¿Qué lenguaje de programación prefieres?	SINGLE
6	Con los desechos de una industria ¿qué se debería realizar?	SINGLE
7	¿Cuáles de estas energías son consideradas limpias?	MULTIPLE
\.


--
-- Data for Name: question_choice; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.question_choice (question_id, choice_id) FROM stdin;
0	0
0	1
0	2
0	3
0	4
0	5
0	6
0	7
0	8
1	0
1	1
1	2
1	3
1	4
1	5
1	6
1	7
1	8
8	0
8	1
8	2
8	3
8	4
8	5
8	6
8	7
8	8
2	9
2	10
2	11
2	12
2	13
2	14
3	15
3	16
3	17
4	18
4	19
4	20
4	21
4	22
4	23
4	12
4	24
4	25
4	26
4	13
4	27
5	18
5	19
5	20
5	21
5	22
5	23
5	12
5	24
5	25
5	26
5	13
5	27
6	28
6	29
6	30
7	31
7	32
7	33
7	34
7	35
\.


--
-- Data for Name: question_correct_choice; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.question_correct_choice (question_id, choice_id) FROM stdin;
2	10
2	12
2	13
3	16
6	29
7	32
7	33
\.


--
-- Data for Name: selection; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.selection (id, answer_id, question_id) FROM stdin;
1	1	0
2	1	1
3	1	8
4	2	2
5	2	3
6	3	4
7	3	5
8	4	6
9	4	7
\.


--
-- Data for Name: selection_choice; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.selection_choice (selection_id, choice_id) FROM stdin;
1	1
1	2
1	5
2	7
3	4
4	13
4	14
5	16
6	12
6	13
6	21
7	13
8	30
9	32
9	33
\.


--
-- Data for Name: survey; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.survey (id, name, type) FROM stdin;
0	alimentos	STATISTICS
1	trivia de java	EVALUATION
2	lenguajes de programación	STATISTICS
3	medio ambiente	EVALUATION
\.


--
-- Data for Name: survey_question; Type: TABLE DATA; Schema: public; Owner: cpci
--

COPY public.survey_question (survey_id, question_id) FROM stdin;
0	0
0	1
0	8
1	2
1	3
2	4
2	5
3	6
3	7
\.


--
-- Name: answer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpci
--

SELECT pg_catalog.setval('public.answer_id_seq', 4, true);


--
-- Name: choice_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpci
--

SELECT pg_catalog.setval('public.choice_id_seq', 36, true);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: cpci
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);


--
-- Name: question_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpci
--

SELECT pg_catalog.setval('public.question_id_seq', 9, true);


--
-- Name: selection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpci
--

SELECT pg_catalog.setval('public.selection_id_seq', 9, true);


--
-- Name: survey_id_seq; Type: SEQUENCE SET; Schema: public; Owner: cpci
--

SELECT pg_catalog.setval('public.survey_id_seq', 4, true);


--
-- Name: answer answer_id_pk; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_id_pk PRIMARY KEY (id);


--
-- Name: answer answer_name_un; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_name_un UNIQUE (name);


--
-- Name: choice choice_id_pk; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.choice
    ADD CONSTRAINT choice_id_pk PRIMARY KEY (id);


--
-- Name: choice choice_text_un; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.choice
    ADD CONSTRAINT choice_text_un UNIQUE (text);


--
-- Name: question question_id_pk; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_id_pk PRIMARY KEY (id);


--
-- Name: question question_text_un; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question
    ADD CONSTRAINT question_text_un UNIQUE (text);


--
-- Name: selection selection_id_pk; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.selection
    ADD CONSTRAINT selection_id_pk PRIMARY KEY (id);


--
-- Name: survey survey_id_pk; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_id_pk PRIMARY KEY (id);


--
-- Name: survey survey_name_un; Type: CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.survey
    ADD CONSTRAINT survey_name_un UNIQUE (name);


--
-- Name: answer answer_survey_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.answer
    ADD CONSTRAINT answer_survey_id_fk FOREIGN KEY (survey_id) REFERENCES public.survey(id);


--
-- Name: question_choice question_choice_choice_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question_choice
    ADD CONSTRAINT question_choice_choice_id_fk FOREIGN KEY (choice_id) REFERENCES public.choice(id);


--
-- Name: question_choice question_choice_question_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question_choice
    ADD CONSTRAINT question_choice_question_id_fk FOREIGN KEY (question_id) REFERENCES public.question(id);


--
-- Name: question_correct_choice question_correct_choice_choice_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question_correct_choice
    ADD CONSTRAINT question_correct_choice_choice_id_fk FOREIGN KEY (choice_id) REFERENCES public.choice(id);


--
-- Name: question_correct_choice question_correct_choice_question_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.question_correct_choice
    ADD CONSTRAINT question_correct_choice_question_id_fk FOREIGN KEY (question_id) REFERENCES public.question(id);


--
-- Name: selection selection_answer_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.selection
    ADD CONSTRAINT selection_answer_id_fk FOREIGN KEY (answer_id) REFERENCES public.answer(id);


--
-- Name: selection_choice selection_choice_choice_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.selection_choice
    ADD CONSTRAINT selection_choice_choice_id_fk FOREIGN KEY (choice_id) REFERENCES public.choice(id);


--
-- Name: selection_choice selection_choice_selection_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.selection_choice
    ADD CONSTRAINT selection_choice_selection_id_fk FOREIGN KEY (selection_id) REFERENCES public.selection(id);


--
-- Name: selection selection_question_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.selection
    ADD CONSTRAINT selection_question_id_fk FOREIGN KEY (question_id) REFERENCES public.question(id);


--
-- Name: survey_question survey_question_question_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.survey_question
    ADD CONSTRAINT survey_question_question_id_fk FOREIGN KEY (question_id) REFERENCES public.question(id);


--
-- Name: survey_question survey_question_survey_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: cpci
--

ALTER TABLE ONLY public.survey_question
    ADD CONSTRAINT survey_question_survey_id_fk FOREIGN KEY (survey_id) REFERENCES public.survey(id);


--
-- PostgreSQL database dump complete
--

