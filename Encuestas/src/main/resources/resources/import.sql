INSERT INTO choice (id, text) VALUES (0, 'Verduras');
INSERT INTO choice (id, text) VALUES (1, 'Frutas');
INSERT INTO choice (id, text) VALUES (2, 'Carne');
INSERT INTO choice (id, text) VALUES (3, 'Pescado y marisco');
INSERT INTO choice (id, text) VALUES (4, 'Productos lácteos');
INSERT INTO choice (id, text) VALUES (5, 'Semillas y frutos secos');
INSERT INTO choice (id, text) VALUES (6, 'Aceites y grasas');
INSERT INTO choice (id, text) VALUES (7, 'Cereales');
INSERT INTO choice (id, text) VALUES (8, 'Tubérculos y Legumbres');
INSERT INTO choice (id, text) VALUES (9, 'HTML');
INSERT INTO choice (id, text) VALUES (10, 'C++');
INSERT INTO choice (id, text) VALUES (11, 'CSS');
INSERT INTO choice (id, text) VALUES (12, 'Java');
INSERT INTO choice (id, text) VALUES (13, 'Python');
INSERT INTO choice (id, text) VALUES (14, 'LaTeX');
INSERT INTO choice (id, text) VALUES (15, 'lanzar un error');
INSERT INTO choice (id, text) VALUES (16, 'probar una condición');
INSERT INTO choice (id, text) VALUES (17, 'imprimir por consola un mensaje');
INSERT INTO choice (id, text) VALUES (18, 'ASM');
INSERT INTO choice (id, text) VALUES (19, 'BASIC');
INSERT INTO choice (id, text) VALUES (20, 'C#');
INSERT INTO choice (id, text) VALUES (21, 'C/C++');
INSERT INTO choice (id, text) VALUES (22, 'Cobol');
INSERT INTO choice (id, text) VALUES (23, 'Fortran');
INSERT INTO choice (id, text) VALUES (24, 'JavaScript');
INSERT INTO choice (id, text) VALUES (25, 'Pascal');
INSERT INTO choice (id, text) VALUES (26, 'PHP');
INSERT INTO choice (id, text) VALUES (27, 'Smalltalk');
INSERT INTO choice (id, text) VALUES (28, 'Verterlos a los rios, ya que estos tienen una capacidad purificadora');
INSERT INTO choice (id, text) VALUES (29, 'Tratarlo en planta y luego disponerlos de forma adecuada');
INSERT INTO choice (id, text) VALUES (30, 'Llamar a la policia, ellos sabrán que hacer');
INSERT INTO choice (id, text) VALUES (31, 'Gasolina');
INSERT INTO choice (id, text) VALUES (32, 'Fotovoltaica');
INSERT INTO choice (id, text) VALUES (33, 'Eólica');
INSERT INTO choice (id, text) VALUES (34, 'Plantas de carbón');
INSERT INTO choice (id, text) VALUES (35, 'Diesel');

INSERT INTO question (id, text, type) VALUES (0, '¿Cuál de estos alimentos sueles consumir?', 'MULTIPLE');
INSERT INTO question (id, text, type) VALUES (1, '¿Cuál de estos alimentos elegirías hoy?', 'SINGLE');
INSERT INTO question (id, text, type) VALUES (2, '¿Cuáles de los siguientes lenguajes son de programación?', 'MULTIPLE');
INSERT INTO question (id, text, type) VALUES (3, 'Un ''if'' ¿sirve para...?', 'SINGLE');
INSERT INTO question (id, text, type) VALUES (4, '¿Qué lenguaje de programación conoces?', 'MULTIPLE');
INSERT INTO question (id, text, type) VALUES (5, '¿Qué lenguaje de programación prefieres?', 'SINGLE');
INSERT INTO question (id, text, type) VALUES (6, 'Con los desechos de una industria ¿qué se debería realizar?', 'SINGLE');
INSERT INTO question (id, text, type) VALUES (7, '¿Cuáles de estas energías son consideradas limpias?', 'MULTIPLE');
INSERT INTO question (id, text, type) VALUES (8, '¿Qué alimentos no te gustan?', 'MULTIPLE');

INSERT INTO survey (id, name, type) VALUES (0, 'alimentos', 'STATISTICS');
INSERT INTO survey (id, name, type) VALUES (1, 'trivia de java', 'EVALUATION');
INSERT INTO survey (id, name, type) VALUES (2, 'lenguajes de programación', 'STATISTICS');
INSERT INTO survey (id, name, type) VALUES (3, 'medio ambiente', 'EVALUATION');

INSERT INTO question_choice (question_id, choice_id) VALUES (0, 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 2);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 3);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 4);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 5);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 6);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 7);
INSERT INTO question_choice (question_id, choice_id) VALUES (0, 8);

INSERT INTO question_choice (question_id, choice_id) VALUES (1, 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 2);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 3);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 4);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 5);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 6);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 7);
INSERT INTO question_choice (question_id, choice_id) VALUES (1, 8);

INSERT INTO question_choice (question_id, choice_id) VALUES (2, 9);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 10);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 11);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 12);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 13);
INSERT INTO question_choice (question_id, choice_id) VALUES (2, 14);

INSERT INTO question_choice (question_id, choice_id) VALUES (4, 12);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 13);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 18);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 19);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 20);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 21);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 22);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 23);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 24);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 25);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 26);
INSERT INTO question_choice (question_id, choice_id) VALUES (4, 27);

INSERT INTO question_choice (question_id, choice_id) VALUES (5, 12);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 13);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 18);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 19);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 20);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 21);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 22);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 23);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 24);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 25);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 26);
INSERT INTO question_choice (question_id, choice_id) VALUES (5, 27);

INSERT INTO question_choice (question_id, choice_id) VALUES (6, 28);
INSERT INTO question_choice (question_id, choice_id) VALUES (6, 29);
INSERT INTO question_choice (question_id, choice_id) VALUES (6, 30);

INSERT INTO question_choice (question_id, choice_id) VALUES (7, 31);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 32);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 33);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 34);
INSERT INTO question_choice (question_id, choice_id) VALUES (7, 35);

INSERT INTO question_choice (question_id, choice_id) VALUES (3, 15);
INSERT INTO question_choice (question_id, choice_id) VALUES (3, 16);
INSERT INTO question_choice (question_id, choice_id) VALUES (3, 17);

INSERT INTO question_choice (question_id, choice_id) VALUES (8, 0);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 1);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 2);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 3);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 4);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 5);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 6);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 7);
INSERT INTO question_choice (question_id, choice_id) VALUES (8, 8);

INSERT INTO question_correct_choice (question_id, choice_id) VALUES (2, 10);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (2, 12);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (2, 13);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (3, 16);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (6, 29);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (7, 32);
INSERT INTO question_correct_choice (question_id, choice_id) VALUES (7, 33);

INSERT INTO survey_question (survey_id, question_id) VALUES (0, 0);
INSERT INTO survey_question (survey_id, question_id) VALUES (0, 1);
INSERT INTO survey_question (survey_id, question_id) VALUES (0, 8);

INSERT INTO survey_question (survey_id, question_id) VALUES (1, 2);
INSERT INTO survey_question (survey_id, question_id) VALUES (1, 3);

INSERT INTO survey_question (survey_id, question_id) VALUES (2, 4);
INSERT INTO survey_question (survey_id, question_id) VALUES (2, 5);

INSERT INTO survey_question (survey_id, question_id) VALUES (3, 6);
INSERT INTO survey_question (survey_id, question_id) VALUES (3, 7);

INSERT INTO answer (id, name, author, datetime, survey_id) VALUES (1, 'alimentos 1', 'Leonardo', '2021-02-05 20:58:48.447', 0);
INSERT INTO answer (id, name, author, datetime, survey_id) VALUES (2, 'trivia de java 1', 'Leonardo', '2021-02-05 20:59:36.995', 1);
INSERT INTO answer (id, name, author, datetime, survey_id) VALUES (3, 'lenguajes de programación 1', 'Leonardo', '2021-02-05 21:00:26.398', 2);
INSERT INTO answer (id, name, author, datetime, survey_id) VALUES (4, 'medio ambiente 1', 'Leonardo', '2021-02-05 21:01:05.392', 3);

INSERT INTO selection (id, answer_id, question_id) VALUES (1, 1, 0);
INSERT INTO selection (id, answer_id, question_id) VALUES (2, 1, 1);
INSERT INTO selection (id, answer_id, question_id) VALUES (9, 1, 8);
INSERT INTO selection (id, answer_id, question_id) VALUES (3, 2, 2);
INSERT INTO selection (id, answer_id, question_id) VALUES (4, 2, 3);
INSERT INTO selection (id, answer_id, question_id) VALUES (5, 3, 4);
INSERT INTO selection (id, answer_id, question_id) VALUES (6, 3, 5);
INSERT INTO selection (id, answer_id, question_id) VALUES (7, 4, 6);
INSERT INTO selection (id, answer_id, question_id) VALUES (8, 4, 7);

INSERT INTO selection_choice (selection_id, choice_id) VALUES (1, 1);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (1, 2);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (1, 5);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (2, 7);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (9, 4);

INSERT INTO selection_choice (selection_id, choice_id) VALUES (3, 13);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (3, 14);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (4, 16);

INSERT INTO selection_choice (selection_id, choice_id) VALUES (5, 21);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (5, 12);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (5, 13);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (6, 13);

INSERT INTO selection_choice (selection_id, choice_id) VALUES (7, 30);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (8, 32);
INSERT INTO selection_choice (selection_id, choice_id) VALUES (8, 33);

SELECT pg_catalog.setval('choice_id_seq', 36, true);
SELECT pg_catalog.setval('question_id_seq', 9, true);
SELECT pg_catalog.setval('survey_id_seq', 4, true);
SELECT pg_catalog.setval('answer_id_seq', 5, true);
SELECT pg_catalog.setval('selection_id_seq', 9, true);