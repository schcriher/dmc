(function() {
    "use strict";

    const isValid = 'is-valid';
    const isInvalid = 'is-invalid';
    const isChecking = 'is-checking';

    let beacon;

    window.addEventListener('load', function() {

        const inputAnswer = document.getElementById('inputAnswer');
        const answerButton = document.getElementById('answerButton');

        if (inputAnswer !== null) {
            const invalidAnswerFeedback = document.getElementById('invalidAnswerFeedback');
            const validateAnswerName = function() {
                const inputAnswerName = inputAnswer.value || '';
                if (inputAnswerName === '') {
                    checking(isInvalid);
                    invalidAnswerFeedback.textContent = 'Debe ingresar un nombre';
                } else {
                    checking(isChecking);
                    $.ajax({
                        url: `/answer/${inputAnswerName}/reserve`,
                        method: 'GET',
                        dataType: 'json',
                        success(data) {
                            checking(isValid);
                            beacon = setInterval(function() {
                                $.ajax({
                                    url: data.reserve.refresh_url,
                                    method: data.reserve.refresh_method,
                                    error() {
                                        checking(isInvalid);
                                        invalidAnswerFeedback.textContent =
                                            'El nombre ya no está disponible, elija otro';
                                    },
                                });
                            }, data.reserve.refresh_millisecond);
                        },
                        error() {
                            checking(isInvalid);
                            invalidAnswerFeedback.textContent = 'El nombre ya existe, elija otro';
                        },
                    });
                }
            };
            // 'mouseleave' event does not work on tablets and mobiles
            inputAnswer.addEventListener('change', validateAnswerName, false);
        }

        const checking = function(status) {
            if (status === isChecking) {
                answerButton.title = 'Comprobando la disponibilidad del nombre de la respuesta';
                answerButton.setAttribute('disabled', null);
                inputAnswer.setAttribute('disabled', null);
            } else {
                answerButton.removeAttribute('disabled');
                inputAnswer.removeAttribute('disabled');
                if (status === isValid) {
                    answerButton.title = 'Hay campos sin rellenar o con errores';
                    inputAnswer.setCustomValidity('');
                } else {
                    answerButton.title = 'Hay campos sin rellenar o con errores';
                    inputAnswer.setCustomValidity('name already exists');
                }
            }
            inputAnswer.classList.remove(isValid);
            inputAnswer.classList.remove(isInvalid);
            inputAnswer.classList.remove(isChecking);
            inputAnswer.classList.add(status);
            clearInterval(beacon);
        };

        const forms = document.getElementsByClassName('needs-validation');
        Array.prototype.filter.call(forms, function(form) {
            form.setAttribute('novalidate', true);
            form.addEventListener('submit', function(event) {
                answerButton.setAttribute('disabled', null);

                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                    answerButton.removeAttribute('disabled');
                    answerButton.title = 'Hay campos sin rellenar o con errores';
                } else {
                    clearInterval(beacon);
                }
                form.classList.add('was-validated');

            }, false);
        });

        document.getElementById('site-body').classList.remove('d-none');
        document.getElementById('site-loader').classList.add('d-none');

    }, false);
})();
