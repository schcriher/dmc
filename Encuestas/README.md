# Actividad: Encuestas

| Implementar un programa que permita mostrar encuestas para ser respondidas por usuarios |
| --- |

## Características

- [x] Las encuestas pueden ser del tipo estadísticas o evaluatorias
- [x] Las preguntas pueden ser del tipo opción simple o múltiple,
      en este último caso pueden quedar sin elegir

## Almacenamiento
- [x] Archivos de texto
- [x] Archivos en formato JSON
- [x] Base de datos a través de JDBC (SQL)
- [x] Base de datos a través de JPA (Hibernate)

## Interfases de Usuario
- [x] Por consola (CLI)
- [x] A través de una web (GUI) con SparkJava + Ajax
- [ ] A través de una web (GUI) con SpringBoot + React

<br>

# CLI: cliente de consola

Para seleccionar el método de almacenamiento se utilizan variables de entorno
(ejemplos `src/main/resources/resources/environment`) del siguiente modo:

- Para archivos de texto\
  `ENCUESTAS_SURVEY_TXT_DIRECTORY=/path/to/surveys`\
  `ENCUESTAS_ANSWER_TXT_DIRECTORY=/path/to/answers`

- Para archivos en formato JSON\
  `ENCUESTAS_SURVEY_JSON_DIRECTORY=/path/to/surveys`\
  `ENCUESTAS_ANSWER_JSON_DIRECTORY=/path/to/answers`

- Para base de datos a través de JDBC\
  `ENCUESTAS_DATABASE_JDBC_URI=jdbc:DRIVER://HOST:PORT/DBNAME?user=USER&password=PASS`

- Para base de datos a través de JPA (Hibernate)\
  `ENCUESTAS_DATABASE_JPA_USE=true`\
  `JDBC_DATABASE_URL=jdbc:DRIVER://HOST:PORT/DBNAME`\
  `JDBC_DATABASE_USERNAME=USER`\
  `JDBC_DATABASE_PASSWORD=PASS`

Para ejecutar la aplicación es necesario compilar y empaquetar el código con maven,
usando `mvn clean package` dentro del proyecto, y luego ejecutar (en sistemas linux):


- Para archivos de texto:
```
ENCUESTAS_SURVEY_TXT_DIRECTORY=target/classes/resources/surveys \
ENCUESTAS_ANSWER_TXT_DIRECTORY=target/classes/resources/answers \
java -cp target/encuestas-1.0-jar-with-dependencies.jar \
com.gitlab.schcriher.dmc.encuestas.viewers.cli.App alimentos alimentos
```

- Para archivos en formato JSON:
```
ENCUESTAS_SURVEY_JSON_DIRECTORY=target/classes/resources/surveys \
ENCUESTAS_ANSWER_JSON_DIRECTORY=target/classes/resources/answers \
java -cp target/encuestas-1.0-jar-with-dependencies.jar \
com.gitlab.schcriher.dmc.encuestas.viewers.cli.App alimentos alimentos
```

- Para base de datos a través de JDBC, es necesario cargar encuestas, las de ejemplo
  se encuentran en `src/main/resources/resources/surveys/run_jdbc.sql`:

```
ENCUESTAS_DATABASE_JDBC_URI='jdbc:DRIVER://HOST:PORT/DBNAME?user=USER&password=PASS' \
java -cp target/encuestas-1.0-jar-with-dependencies.jar \
com.gitlab.schcriher.dmc.encuestas.viewers.cli.App alimentos alimentos
```
El esquema de la base de datos es:\
![jdbc](img/jdbc.png)


- Para base de datos a través de JPA (Hibernate), no es necesario cargar los datos de ejemplo
  del archivo `src/main/resources/resources/surveys/run_jpa.sql` ya que se cargan automáticamente
  los datos del archivo `src/main/resources/resources/import.sql` que es necesario para la versión
  web:
```
ENCUESTAS_DATABASE_JPA_USE=true \
JDBC_DATABASE_URL='jdbc:DRIVER://HOST:PORT/DBNAME' \
JDBC_DATABASE_USERNAME=USER \
JDBC_DATABASE_PASSWORD=PASS \
java -cp target/encuestas-1.0-jar-with-dependencies.jar \
com.gitlab.schcriher.dmc.encuestas.viewers.cli.App alimentos alimentos
```
El esquema de la base de datos es:\
![jpa](img/jpa.png)

<br>

# GUI: cliente web

Para el cliente web se reutilizó la opción de guardado en base de datos con JPA, dado que la
plataform elegida es Heroku en su tier gratuito, la web se reinicia al menos una vez al día,
con lo cual en el inicio de la misma se cargar datos de ejemplos, del archivo
`src/main/resources/resources/import.sql`.

## Con SparkJava + Ajax

URL: https://dmc-encuestas-spark.herokuapp.com/

[comment]: <> (## Con SpringBoot + React)
[comment]: <> (URL: https://dmc-encuestas-spring.herokuapp.com/)
